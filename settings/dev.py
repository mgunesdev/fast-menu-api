"""
Django settings for development.

"""
from settings.base import *

DEBUG = True

DATABASES = {
    'default': {
        'NAME': env('DEFAULT_DATABASE_NAME'),
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'USER': env('DEFAULT_DATABASE_USER'),
        'PASSWORD': env('DEFAULT_DATABASE_PASSWORD'),
        'HOST': env('DEFAULT_DATABASE_HOST'),
        'PORT': env('DEFAULT_DATABASE_PORT'),
    }
}

