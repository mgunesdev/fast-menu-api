"""
Django settings for PROD .

"""
import logging
# import sentry_sdk
# from sentry_sdk.integrations.django import DjangoIntegration
# from sentry_sdk.integrations.logging import LoggingIntegration
from settings.base import *

DEBUG = False


# sentry_sdk.init(
#     dsn="https://473084c3e05c438482f0e2b493c7901c@sentry.io/1548135",
#     integrations=[DjangoIntegration(),
#                   LoggingIntegration(
#                       level=logging.INFO,  # Capture info and above as breadcrumbs
#                       event_level=logging.ERROR  # Send no events from log messages
#                   )
#                   ]
# )

DATABASES = {
    'default': {
        'NAME': env('PROD_DATABASE_NAME'),
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'USER': env('PROD_DATABASE_USER'),
        'PASSWORD': env('PROD_DATABASE_PASSWORD'),
        'HOST': env('PROD_DATABASE_HOST'),
        'PORT': env('PROD_DATABASE_PORT'),
    }
}
