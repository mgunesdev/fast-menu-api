# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time

from builtins import float

from datetime import date, timedelta, datetime

from django.contrib.gis.geos import Point, Polygon
from django.contrib.gis.measure import D
from django.db.models import F, Count, Avg, Q, Max
from rest_framework.mixins import ListModelMixin

from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    ListAPIView
)

from app.models import Product

from apps.search.serializers import SearchProductListSerializer, SearchBranchListSerializer, SearchBranchMapSerializer


class SearchProductListAPIView(ListAPIView, ListModelMixin):
    queryset = Product.objects.all()
    serializer_class = SearchProductListSerializer

    def get(self, request, *args, **kwargs):
        return get_detail_successfully_response(self.list(request, *args, **kwargs))

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        return queryset

    def get_queryset(self):
        point = self.request.query_params.get('point', None)
        keyword = self.request.query_params.get('keyword', None)
        criteria = {}
        # if point is not None and "," in point:
        #     point_array = point.split(",")
        #     lat = float(point_array[0])
        #     lng = float(point_array[1])
        #     point = Point(lat, lng)
        #     radius = 2
        #     criteria["menu__branch__location__point__distance_lte"] = (point, D(km=radius))
        if keyword:
            criteria["title__iregex"] = r"(^|\s)%s" % keyword

        top_list = self.get_serializer(data=self.queryset.filter(**criteria)
                                       .annotate(
            avg_rating=Avg('product_comment__rate')
        ).order_by("-avg_rating")[:5], many=True)
        top_list.is_valid()
        sponsored = self.get_serializer(data=self.queryset.filter(**criteria)
                                        .annotate(
            avg_rating=Avg('product_comment__rate')
        ).order_by("-avg_rating")[:5], many=True)
        sponsored.is_valid()
        results = [
            {'title': 'En İyiler', 'products': top_list.data},
            {'title': 'Önerilenler', 'products': sponsored.data}]
        return results


class SearchBranchListAPIView(ListAPIView, ListModelMixin):
    queryset = Product.objects.all()
    serializer_class = SearchBranchListSerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        point = self.request.query_params.get('point', None)
        keyword = self.request.query_params.get('keyword', None)
        geo_polygon_list = self.request.query_params.getlist('geo_polygon[]', None)
        criteria = {}
        # if point is not None and "," in point:
        #     point_array = point.split(",")
        #     lat = float(point_array[0])
        #     lng = float(point_array[1])
        #     point = Point(lat, lng)
        #     radius = 2
        #     criteria["menu__branch__location__point__distance_lte"] = (point, D(km=radius))
        if keyword:
            criteria["title__iregex"] = r"(^|\s)%s" % keyword

        queryset = self.queryset.filter(**criteria).order_by("menu__branch__id").distinct('menu__branch__id')
        return queryset


class SearchBranchMapListAPIView(ListAPIView, ListModelMixin):
    queryset = Branch.objects.all()
    serializer_class = SearchBranchMapSerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        queryset = self.queryset
        geo_polygon_list = self.request.query_params.getlist('geo_polygon[]', None)
        rate = self.request.query_params.get('rate', None)
        property = self.request.query_params.get('property', None)
        working_status = self.request.query_params.get('working_status', None)
        keyword = self.request.query_params.get('keyword', None)
        point = self.request.query_params.get('point', None)

        if keyword:
            queryset = queryset.filter(name__istartswith=keyword)

        if point is not None and "," in point:
            point_array = point.split(",")
            lat = float(point_array[0])
            lng = float(point_array[1])
            point = Point(lat, lng)
            radius = 100
            queryset = queryset.filter(location__point__distance_lte=(point, D(km=radius)))
        elif geo_polygon_list:
            coord = []
            first_point_array = geo_polygon_list[0].split(",")
            for loc in geo_polygon_list:
                loc_list = loc.split(",")
                coord.append((float(loc_list[0]), float(loc_list[1])))

            coord.append((float(first_point_array[0]), float(first_point_array[1])))
            polygon = Polygon(coord)
            queryset = queryset.filter(location__point__within=polygon)

            if property:
                if rate:
                    queryset = queryset.filter(property_branch__property__code__in=property.split(","))
                else:
                    queryset = queryset.filter(property_branch__property__code__in=property.split(",")).distinct('id')

            if rate:
                queryset = queryset.annotate(
                    avg_rating=Avg('branch_comment_branch__rate'),
                    count=Count('id', distinct=True)
                ).filter(
                    branch_comment_branch__rate__gte=rate)

            if working_status:
                branch_list = []
                today = date.today()
                current_day = today.weekday() + 1
                for branch in queryset.all():
                    working_time = branch.working_time
                    if working_time:
                        import ast
                        day = ast.literal_eval(working_time)[str(current_day)]
                        if day == "Off":
                            branch_list.append(branch.id)
                        else:
                            time_array = day.split("-")
                            opening_time = time_array[0]
                            closing_time = time_array[1]

                            opening_array = opening_time.split(":")
                            opening_hour = int(opening_array[0])
                            opening_minute = int(opening_array[1])

                            closing_array = closing_time.split(":")
                            closing_hour = int(closing_array[0])
                            closing_minute = int(closing_array[1])

                            opening_date = datetime(today.year, today.month, today.day, opening_hour, opening_minute)
                            closing_date = datetime(today.year, today.month, today.day, closing_hour, closing_minute)

                            if closing_hour < opening_hour:
                                current_hour = datetime.today().hour
                                if current_hour < opening_hour:
                                    opening_date = opening_date - timedelta(days=1)
                                elif current_hour > closing_hour:
                                    closing_date = closing_date + timedelta(days=1)

                            opening_time_milis = int(round(opening_date.timestamp() * 1000))
                            closing_time_milis = int(round(closing_date.timestamp() * 1000))

                            current_milis = int(round(time.time() * 1000))
                            if opening_time_milis < current_milis < closing_time_milis:
                                pass
                            else:
                                branch_list.append(branch.id)
                    else:
                        branch_list.append(branch.id)

                queryset = queryset.exclude(id__in=branch_list)

        return queryset
