# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from django.db.models import Avg
from django.contrib.auth.models import AnonymousUser
from rest_framework import serializers

from app.models import Product, ProductImage, Company, ProductComment, ProductFav, Price, Branch, BranchProperty, \
    BranchComment, Menu
from apps.branch.branchImage.serializers import BranchImageSerializer
from apps.location.serializers import LocationSerializer
from apps.product.price.serializers import PriceSerializer
from apps.product.productImage.serializers import ProductImageSerializer
from apps.property.serializers import PropertySerializer


class SearchProductListSerializer(ModelSerializer):
    price = SerializerMethodField()
    average_rate = SerializerMethodField()
    company = SerializerMethodField()
    images = SerializerMethodField()
    fav = SerializerMethodField()

    class Meta:
        model = Product
        fields = ['id',
                  'menu',
                  'title',
                  'order',
                  'price',
                  'average_rate',
                  'images',
                  'fav',
                  'company',
                  'status'
                  ]

    def get_price(self, obj):
        price = Price.objects.filter(product_id=obj.id).first()
        if price is None:
            return None
        return PriceSerializer(price).data

    def get_average_rate(self, obj):
        average_rate = ProductComment.objects.filter(product_id=obj.id, parent=None).aggregate(Avg('rate')).get(
            "rate__avg")
        if average_rate is not None:
            return round(
                average_rate, 1)
        return None

    def get_company(self, obj):
        return {"name": Company.objects.get(id=obj.menu.branch.company.id).name}

    def get_images(self, obj):
        c_qs = ProductImage.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        images = ProductImageSerializer(c_qs, many=True).data
        return images

    def get_fav(self, obj):
        if type(self.context['request'].user) is not AnonymousUser:
            try:
                fav = ProductFav.objects.get(product_id=obj.id, user_id=self.context['request'].user.id)
                return {"id": fav.id}
            except Exception:
                pass

        return None


class SearchBranchListSerializer(ModelSerializer):
    price = SerializerMethodField()
    branch = SerializerMethodField()
    images = SerializerMethodField()
    average_rate = SerializerMethodField()

    class Meta:
        model = Product
        fields = ['id',
                  'title',
                  'order',
                  'price',
                  'images',
                  'branch',
                  'status',
                  'average_rate',
                  ]

    def get_average_rate(self, obj):
        average_rate = ProductComment.objects.filter(product_id=obj.id, parent=None).aggregate(Avg('rate')).get(
            "rate__avg")
        if average_rate is not None:
            return round(
                average_rate, 1)
        return None

    def get_price(self, obj):
        price = Price.objects.filter(product_id=obj.id).first()
        if price is None:
            return None
        return PriceSerializer(price).data

    def get_branch(self, obj):
        return {"name": Branch.objects.get(id=obj.menu.branch.id).name, "id": obj.menu.branch.id}

    def get_images(self, obj):
        c_qs = ProductImage.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        images = ProductImageSerializer(c_qs, many=True).data
        return images


class SearchBranchMapSerializer(ModelSerializer):
    images = BranchImageSerializer(source="branch_image", many=True, read_only=True)
    location = LocationSerializer()
    properties = SerializerMethodField()
    average_rate = SerializerMethodField()
    working_time = SerializerMethodField()

    class Meta:
        model = Branch
        fields = ['id',
                  'name',
                  'images',
                  'properties',
                  "average_rate",
                  'location',
                  'working_time'
                  ]

    def get_properties(self, obj):
        c_qs = BranchProperty.objects.filter(branch_id=obj.id)
        if c_qs and len(c_qs) > 0:
            property_list = []
            for branch_property in c_qs:
                property_list.append(PropertySerializer(branch_property.property).data)
            return property_list
        else:
            return None

    def get_average_rate(self, obj):
        rate = BranchComment.objects.filter(branch_id=obj.id, parent=None).aggregate(Avg('rate')).get("rate__avg")
        if rate is not None:
            return round(
                rate, 1)

        return 0

    def get_working_time(self, obj):
        working_json = {}
        summary_json = {}
        resutl_summary = []
        try:
            days = json.loads(obj.working_time)
            working_json["days"] = days

            for day, value in days.items():
                if value not in summary_json:
                    summary_json[value] = []

                summary_json[value].append(day)

            for time, days in summary_json.items():
                resutl_summary.append("{}/{}".format(time, ",".join(days)))

            working_json["summary"] = resutl_summary
            return working_json
        except Exception:
            return None
