# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.search.views import (
    SearchProductListAPIView,
    SearchBranchListAPIView,
    SearchBranchMapListAPIView
)

urlpatterns = [
    url(r'^product/', SearchProductListAPIView.as_view(), name='api-search-product'),
    url(r'^branch/', SearchBranchListAPIView.as_view(), name='api-search-branch'),
    url(r'^map/', SearchBranchMapListAPIView.as_view(), name='api-search-map')

]
