from apps.branch.branchPhoneNumber.serializers import BranchPhoneNumberSerializer
from apps.location.serializers import LocationSerializer
from apps.phoneNumber.serializers import PhoneNumberListSerializer
from app.models import PhoneNumber, BranchPhoneNumber, Location


def get_short_user_model(self, user):
    response = {}
    request = self.context.get('request')

    if user.id:
        response["id"] = user.id

    if user.name_surname:
        response["name_surname"] = user.name_surname

    phone_numbers = PhoneNumber.objects.filter(user_id=user.id)
    if len(phone_numbers) > 0:
        response["phone_number"] = PhoneNumberListSerializer(phone_numbers, many=True).data

    if user.image:
        response["image"] = user.image.url

    return response


def get_short_company_model(self, company):
    request = self.context.get('request')
    photo_url = company.logo.url
    request.build_absolute_uri(photo_url)

    return {"id": company.id,
            "name": company.name,
            "logo": request.build_absolute_uri(photo_url)}


def get_short_branch_model(self, branch):
    phone_numbers = BranchPhoneNumber.objects.filter(branch_id=branch.id)

    return {"id": branch.id,
            "name": branch.name,
            "phones": BranchPhoneNumberSerializer(phone_numbers, many=True).data,
            "company": get_short_company_model(self, branch.company)
            }


def get_short_branch_model_with_location(branch):
    location_map = LocationSerializer(branch.location).data
    location = {"city": {'name': "{}".format(location_map["city"]["name"])},
                "district": {'name': "{}".format(location_map["district"]["name"])}}

    return {"id": branch.id,
            "name": branch.name,
            "location": location
            }
