class ResponseInfo(object):
    ERROR_CODE = "errorCode"
    ERROR_MESSAGE = "errorMessage"
    SUCCESS_MESSAGE = "successMessage"
    DATA = "data"
    PAGE = "page"
    TOTAL_ITEMS = "totalItems"

    def __init__(self, user=None, **args):
        self.response = {
            "errorCode": args.get('status', None),
            "errorMessage": args.get('error', None),
            "successMessage": args.get('success', None),
            "data": args.get('data', None),
            "totalItems": args.get('totalItems', None)
        }
