# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_401_UNAUTHORIZED, \
    HTTP_403_FORBIDDEN, \
    HTTP_404_NOT_FOUND, HTTP_200_OK, HTTP_501_NOT_IMPLEMENTED, HTTP_409_CONFLICT

from app.models import (
    User,
    Menu,
    Company,
    CompanyImage,
    BranchImage,
    BranchPhoneNumber,
    Branch,
    BranchComment,
    BranchFollow,
    Category,
    DiscountCode,
    Feedback,
    PhoneNumber,
    ProductComment,
    ProductFav,
    PreparingTime,
    Price, ProductImage, Product, Campaign)
from apps.helper.responseProcess.ResponseInfo import *


class SuccessMessages:
    DELETED_SUCCESSFULLY = "Başarı ile silindi!"
    UPDATED_SUCCESSFULLY = "Başarı ile güncellendi!"
    CREATED_SUCCESSFULLY = "Başarı ile oluşturuldu!"


class ErrorMessages:
    ERROR_UNKNOWN = "Bilinmeyen bir hata oluştu!"
    ERROR_NOT_FOUND = "Böyle bir kayıt bulunamadı!"
    ERROR_NOT_OWNER = "Bu işlemi yapmaya yetkiniz yok!"
    ERROR_NOT_LOGIN = "Bu işlemi yapabilmek için giriş yapmalısınız!"
    ERROR_MISSING_INFO = "Eksik alan!"
    ERROR_ALREADY_DONE = "Bu işlem zaten yapılmış!"


class ErrorCodes:
    NOT_LOGIN_CODE = 10001
    NOT_OWNER = 10002
    NOT_FOUND = 10003
    BAD_REQUEST = 10004
    UNKNOWN = 10005
    CONFLICT = 1006


def is_authenticated(request):
    return bool(request.user and request.user.is_authenticated)


def check_owner_for_model(request, model):
    model_type = type(model)
    if model_type is Menu and request.user == model.branch.user:
        return True

    if model_type is Company and request.user == model.user:
        return True

    if model_type is Branch and request.user == model.user:
        return True

    if model_type is CompanyImage and request.user == model.company.user:
        return True

    if model_type is BranchImage and request.user == model.branch.user:
        return True

    if model_type is BranchPhoneNumber and request.user == model.branch.user:
        return True

    if model_type is BranchComment and request.user == model.user:
        return True

    if model_type is BranchFollow and request.user == model.user:
        return True

    if model_type is Category and request.user == model.menu.branch.user:
        return True

    if model_type is DiscountCode and request.user == model.branch.user:
        return True

    if model_type is Feedback and request.user == model.user:
        return True

    if model_type is PhoneNumber and request.user == model.user:
        return True

    if model_type is ProductComment and request.user == model.user:
        return True

    if model_type is ProductFav and request.user == model.user:
        return True

    if model_type is PreparingTime and request.user == model.product.menu.user:
        return True

    if model_type is Price and request.user == model.product.menu.user:
        return True

    if model_type is Product and request.user == model.menu.branch.user:
        return True

    if model_type is ProductImage and request.user == model.product.menu.branch.user:
        return True

    if model_type is Campaign and request.user == model.product.menu.branch.user:
        return True

    return False


def is_owner(request, model):
    if type(model) is User:
        return model == request.user or model == request.user.parent
    return request.user == model.user or model.user == request.user.parent


def get_not_authenticated_response():
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = ErrorMessages.ERROR_NOT_LOGIN
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.NOT_LOGIN_CODE

    return Response(response, status=HTTP_401_UNAUTHORIZED)


def get_not_owner_response():
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = ErrorMessages.ERROR_NOT_OWNER
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.NOT_OWNER

    return Response(response, status=HTTP_403_FORBIDDEN)


def get_not_found_response():
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = ErrorMessages.ERROR_NOT_FOUND
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.NOT_FOUND

    return Response(response, status=HTTP_400_BAD_REQUEST)


def get_bad_request_response(exception):
    exp_list = list(exception.detail.keys())
    title = exp_list[0]
    message = exception.detail.get(title)[0]
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = "{}: {}".format(title, message)
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.BAD_REQUEST

    return Response(response, status=HTTP_400_BAD_REQUEST)


def get_bad_request_key_response(key):
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = "{}: {}".format(key, ErrorMessages.ERROR_MISSING_INFO)
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.BAD_REQUEST

    return Response(response, status=HTTP_400_BAD_REQUEST)


def get_bad_request_message_response(message):
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = message
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.BAD_REQUEST

    return Response(response, status=HTTP_400_BAD_REQUEST)


def get_unknown_error_response():
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = ErrorMessages.ERROR_UNKNOWN
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.UNKNOWN

    return Response(response, status=HTTP_501_NOT_IMPLEMENTED)


def get_already_done_response(model, message=ErrorMessages.ERROR_ALREADY_DONE):
    response = ResponseInfo().response
    response[ResponseInfo.ERROR_MESSAGE] = message
    response[ResponseInfo.ERROR_CODE] = ErrorCodes.CONFLICT
    response[ResponseInfo.DATA] = model

    return Response(response, status=HTTP_409_CONFLICT)


def get_deleted_successfully_response():
    response = ResponseInfo().response
    response[ResponseInfo.SUCCESS_MESSAGE] = SuccessMessages.DELETED_SUCCESSFULLY

    return Response(response, status=HTTP_200_OK)


def get_created_successfully_response(model):
    response = ResponseInfo().response
    response[ResponseInfo.SUCCESS_MESSAGE] = SuccessMessages.CREATED_SUCCESSFULLY
    response[ResponseInfo.DATA] = model

    return Response(response, status=HTTP_201_CREATED)


def get_updated_successfully_response(model):
    response = ResponseInfo().response
    response[ResponseInfo.SUCCESS_MESSAGE] = SuccessMessages.UPDATED_SUCCESSFULLY
    response[ResponseInfo.DATA] = model

    return Response(response, status=HTTP_200_OK)


def get_detail_successfully_response(model):
    response = ResponseInfo().response
    response[ResponseInfo.DATA] = model

    return Response(response, status=HTTP_200_OK)


def get_list_successfully_response(params, data):
    response = ResponseInfo().response
    if params.get("page"):
        response[ResponseInfo.PAGE] = params.get("page")
    if data.get('count'):
        response[ResponseInfo.TOTAL_ITEMS] = data.get("count")

    response[ResponseInfo.DATA] = data["results"]

    return Response(response, status=HTTP_200_OK)
