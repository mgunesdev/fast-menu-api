from app.models import City, District, Locality
from math import sin, cos, sqrt, atan2, radians, acos


def point_in_poly(lng, lat, poly):
    n = len(poly)
    inside = False

    p1x, p1y = poly[0]
    for i in range(n + 1):
        p2x, p2y = poly[i % n]
        if lat > min(p1y, p2y):
            if lat <= max(p1y, p2y):
                if lng <= max(p1x, p2x):
                    if p1y != p2y:
                        xints = (lat - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or lng <= xints:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside


def get_city_district_from_latlng(lng, lat):
    city_list = City.objects.all()
    current_city = None
    current_district = None
    current_locality = None

    for city in city_list:
        polygon = city.polygon
        if polygon:
            if point_in_poly(lng, lat, polygon):
                current_city = city
                break

    if current_city:
        district_list = District.objects.filter(parentcode=current_city.code)
        for district in district_list:
            polygon = district.polygon
            if polygon:
                if point_in_poly(lng, lat, polygon):
                    current_district = district
                    break

    if current_district:
        locality_list = Locality.objects.filter(parentcode=current_district.code)
        for locality in locality_list:
            polygon = locality.polygon
            if polygon:
                if point_in_poly(lng, lat, polygon):
                    current_locality = locality
                    break

    return current_city, current_district, current_locality


def get_city_district_from_point(point):
    point_array = point.split(",")
    lat = float(point_array[0])
    lng = float(point_array[1])
    return get_city_district_from_latlng(lng, lat)


def get_distance_between_two_loc_point(lat1, lon1, lat2, lon2):
    R = 6373.0

    lat1 = radians(float(lat1))
    lon1 = radians(float(lon1))
    lat2 = radians(float(lat2))
    lon2 = radians(float(lon2))

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c
