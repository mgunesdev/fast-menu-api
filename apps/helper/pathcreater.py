# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from os.path import join, splitext



def get_file_path(instance, filename):
    from app.models import Menu, Category, ProductImage, User, Company, CompanyImage, BranchImage, QrCode, Campaign
    lastPath = splitext(filename)[1][1:].lower()
    instanceType = type(instance)

    if instanceType is Menu:
        return 'users/{}/company/branch/{}/menu/{}/image/{}'.format(
            str(instance.branch.user.id),
            str(instance.branch.id),
            str(instance.pk),
            filename)

    elif instanceType is Category:
        return 'users/{}/company/branch/{}/menu/{}/category/{}/image/{}'.format(
            str(instance.menu.branch.user.id),
            str(instance.menu.branch.id),
            str(instance.menu.id),
            str(instance.pk),
            filename)
    elif instanceType is ProductImage:
        return 'users/{}/company/branch/{}/menu/{}/category/{}/product/{}/images/{}'.format(
            str(instance.product.menu.branch.user.id),
            str(instance.product.menu.branch.id),
            str(instance.product.menu.id),
            str(instance.product.category.id),
            str(instance.product.pk),
            filename)
    elif instanceType is User:
        return 'users/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Company:
        return 'users/{}/company/logo/{}'.format(str(instance.user.id), filename)
    elif instanceType is CompanyImage:
        return 'users/{}/company/images/{}'.format(str(instance.company.user.id), filename)
    elif instanceType is BranchImage:
        return 'users/{}/company/branch/{}/images/{}'.format(str(instance.branch.user.id), str(instance.branch.id),
                                                             filename)
    elif instanceType is QrCode:
        return "qrcodes/{}".format(filename)
    elif instanceType is Campaign:
        return 'users/{}/company/branch/{}/menu/{}/category/{}/product/{}/campaign/{}'.format(
            str(instance.product.menu.branch.user.id),
            str(instance.product.menu.branch.id),
            str(instance.product.menu.id),
            str(instance.product.category.id),
            str(instance.product.pk),
            filename)

    return join(instance, filename)
