# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def getCapitalizedString(name):
    array = name.split()
    capArray = []

    for word in array:
        capArray.append(word.capitalize())

    return " ".join(capArray)
