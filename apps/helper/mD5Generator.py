import hashlib


def generateMD5(email):
    mHash = hashlib.md5()
    mHash.update(email)
    return mHash.hexdigest()
