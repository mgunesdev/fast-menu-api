# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def checkDataGetSerializer(obj, data):
    serializer = obj.get_serializer(data=data)
    serializer.is_valid(raise_exception=True)
    return serializer
