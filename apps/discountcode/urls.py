# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.discountcode.views import (
    DiscountCodeViewSet
)

discountcode_viewset = DiscountCodeViewSet.as_view({'get': 'list', 'post': 'create'})
discountcode_detail_viewset = DiscountCodeViewSet.as_view({'put': 'update', 'get': 'retrieve'})
urlpatterns = [
    url(r'^$', discountcode_viewset, name='api-discount-code'),
    url(r'^(?P<discount_code>.*)/$', discountcode_detail_viewset, name='api-discount-code-update')
]
