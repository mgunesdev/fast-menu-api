# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.discountcode.views import (
    DiscountCodeCountAPIView
)

urlpatterns = [
    url(r'^$', DiscountCodeCountAPIView.as_view(), name='api-discount-code-count'),
]
