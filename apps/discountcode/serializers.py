# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.password_validation import validate_password
from django.core import exceptions as django_exceptions
from django.db import IntegrityError, transaction
from djoser.conf import settings

from app.models import DiscountCode

from rest_framework import serializers

from rest_framework.serializers import (
    CharField,
    EmailField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError
)

from apps.helper.responseProcess.ModelResponseHelper import get_short_branch_model_with_location


class DiscountCodeSerializer(ModelSerializer):
    class Meta:
        model = DiscountCode
        fields = [
            'id',
            'device_code',
            'branch',
            'qr_code',
            'branch',
            'created_at',
            'updated_at',
        ]

    def to_representation(self, instance):
        representation = super(DiscountCodeSerializer, self).to_representation(instance)
        representation['discount_code'] = "{}-{}".format(instance.discount_code[:4],
                                                         instance.discount_code[4:len(instance.discount_code)])
        representation['status'] = instance.status
        representation['branch'] = get_short_branch_model_with_location(instance.branch)
        representation['device_code'] = None

        return representation


class DiscountCodeUpdateSerializer(ModelSerializer):
    class Meta:
        model = DiscountCode
        fields = [
            'id',
            'status',
            'created_at',
            'updated_at',
        ]

    def to_representation(self, instance):
        representation = super(DiscountCodeUpdateSerializer, self).to_representation(instance)
        representation['discount_code'] = "{}-{}".format(instance.discount_code[:4],
                                                         instance.discount_code[4:len(instance.discount_code)])
        return representation
