# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from builtins import Exception, type

import os

from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView

from app.decorators import check_auth
from apps.core.viewset import CustomViewSet
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)

from .serializers import (
    DiscountCodeSerializer,
    DiscountCodeUpdateSerializer
)

from apps.helper.ApiViewHelper import checkDataGetSerializer
from app.models import DiscountCode, Branch, QrCode
import uuid


class DiscountCodeViewSet(CustomViewSet):
    serializer_class = DiscountCodeSerializer
    queryset = DiscountCode.objects.all()
    permission_classes = (IsAuthenticated,)
    filterset_fields = ['branch', 'qr_code', 'device_code', 'user', 'status']

    def create(self, request, *args, **kwargs):
        data = request.data

        try:
            checkDataGetSerializer(self, data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        try:
            device_code = request.data["device_code"]
            qr_code = QrCode.objects.get(id=request.data["qr_code"])
            branch = Branch.objects.get(id=request.data["branch"])
            user = request.user

            discount_code = DiscountCode.objects.filter(
                qr_code=qr_code,
                branch=branch,
                user=user
            ).first()

            if discount_code:
                return get_already_done_response(self.serializer_class(discount_code).data,
                                                 "Bu mekan için daha önce indirim kodu ürettiniz!")

            discount_code = DiscountCode.objects.filter(
                qr_code=qr_code,
                branch=branch,
                device_code=device_code
            ).first()

            if discount_code:
                return get_already_done_response(self.serializer_class(discount_code).data,
                                                 "Bu mekan için daha önce indirim kodu ürettiniz!")

            if not discount_code:
                discount_code = DiscountCode()
                discount_code.user = user
                discount_code.branch = branch
                discount_code.qr_code = qr_code
                discount_code.discount_code = uuid.uuid4().hex[:8].upper()
                discount_code.device_code = device_code

                discount_code.save()

            return get_detail_successfully_response(self.serializer_class(discount_code).data)

        except Exception:
            response = get_unknown_error_response()

        return response

    def list(self, request, *args, **kwargs):
        request.GET._mutable = True
        branch = request.query_params.get("branch", None)

        if branch:
            self.queryset = self.queryset.filter(branch=branch)
            request.GET.pop("branch")
        else:
            self.queryset = self.queryset.filter(user=request.user)

        return get_list_successfully_response(
            request.GET,
            super(DiscountCodeViewSet, self).list(request, args, kwargs).data
        )

    def update(self, request, *args, **kwargs):
        self.serializer_class = DiscountCodeUpdateSerializer
        try:
            code = kwargs["discount_code"]
            discount_code = DiscountCode.objects.get(discount_code=code)
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, discount_code):
            return get_not_owner_response()

        try:
            status = request.data['status']
            if discount_code.status == 0:
                return get_bad_request_message_response("Daha önce kullanılmış kod!")

            discount_code.status = status
            discount_code.save()
            return get_updated_successfully_response(
                DiscountCodeUpdateSerializer(discount_code).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
        except Exception as exp:
            return get_bad_request_message_response("Eksik ya da yanlış bilgi girdiniz!")


class DiscountCodeCountAPIView(APIView):
    queryset = DiscountCode.objects.all()

    @check_auth
    def get(self, request, format=None):
        branch = request.query_params.get("branch", None)
        status = request.query_params.get("status", None)
        if branch:
            self.queryset = self.queryset.filter(branch=branch)
        else:
            self.queryset = self.queryset.filter(user=request.user)

        if status:
            self.queryset = self.queryset.filter(status=status)

        data = {"count": self.queryset.count(), "results": []}
        return get_list_successfully_response(request.GET, data)
