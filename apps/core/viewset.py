from rest_framework import mixins
from rest_framework.viewsets import ViewSetMixin

from apps.core.views import CustomApiView, CustomRetrieveApiView
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin


class CustomViewSet(
    ViewSetMixin,
    CustomApiView,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    CustomRetrieveApiView,
    DynamicFieldsViewMixin
):
    pass
