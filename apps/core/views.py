from django.contrib.auth.models import AnonymousUser
from rest_framework.exceptions import NotAuthenticated, ValidationError
from rest_framework.generics import (
    GenericAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView)

from apps.helper.responseProcess.ResponseHelper import (
    get_bad_request_message_response,
    get_not_found_response,
    get_detail_successfully_response,
    get_not_authenticated_response, is_authenticated, get_updated_successfully_response, get_bad_request_response,
    is_owner, get_not_owner_response)


class CustomApiView(GenericAPIView):
    def check_permissions(self, request):
        try:
            super(CustomApiView, self).check_permissions(request)
        except NotAuthenticated as e:
            return get_bad_request_message_response(e.default_detail)

    def dispatch(self, request, *args, **kwargs):
        """
        `.dispatch()` is pretty much the same as Django's regular dispatch,
        but with extra hooks for startup, finalize, and exception handling.
        """
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?
        response = None
        try:
            self.initial(request, *args, **kwargs)

            # Get the appropriate handler method
            if request.method.lower() in self.http_method_names:
                handler = getattr(self, request.method.lower(),
                                  self.http_method_not_allowed)
            else:
                handler = self.http_method_not_allowed

            if type(request.user) != AnonymousUser:
                response = handler(request, *args, **kwargs)
            else:
                response = get_bad_request_message_response('Authentication credentials were not provided.')

        except Exception as exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response


class CustomRetrieveApiView(RetrieveAPIView, GenericAPIView):

    def retrieve(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
        except Exception:
            return get_not_found_response()

        return get_detail_successfully_response(
            super(CustomRetrieveApiView, self).retrieve(request, args, kwargs).data
        )


class CustomRetrieveApiView(RetrieveAPIView, GenericAPIView):

    def retrieve(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
        except Exception:
            return get_not_found_response()

        return get_detail_successfully_response(
            super(CustomRetrieveApiView, self).retrieve(request, args, kwargs).data
        )


class CustomRetrieveUpdateApiView(RetrieveUpdateAPIView):

    def put(self, request, *args, **kwargs):
        try:
            object = self.get_object()
        except Exception:
            return get_not_found_response()

        if not is_authenticated(request):
            return get_not_authenticated_response()

        try:
            if getattr(object, 'user') and not is_owner(request, object):
                return get_not_owner_response()
        except:
            pass
        
        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
