# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.menu.views import (
    MenuModelCreateAPIView,
    MenuModelListAPIView,
    MenuModelDeleteAPIView,
    MenuModelUpdateAPIView,
    MenuModelDetailAPIView,
    MyMenuList
)

urlpatterns = [
    url(r'^create/$', MenuModelCreateAPIView.as_view(), name='api-menu-create'),
    url(r'^detail/(?P<pk>\d+)', MenuModelDetailAPIView.as_view(), name='api-menu-detail'),
    url(r'^delete/(?P<pk>\d+)', MenuModelDeleteAPIView.as_view(), name='api-menu-delete'),
    url(r'^list/$', MenuModelListAPIView.as_view(), name='api-menu-list'),
    url(r'^my-menu/$', MyMenuList.as_view(), name='api-my-menu-list'),
    url(r'^edit/(?P<pk>\d+)', MenuModelUpdateAPIView.as_view(), name='api-menu-edit')
]
