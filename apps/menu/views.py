# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from app.models import Menu, Category
from .serializers import (
    MenuModelCreateSerializer,
    MenuModelListSerializer,
    MenuModelDetailSerializer,
    MenuModelUpdateSerializer,
    MenuModelDeleteSerializer
)


class MenuModelCreateAPIView(CreateAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuModelCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        if request.data.get("branch"):

            if not check_owner_for_model(request, Branch.objects.get(id=request.data["branch"])):
                return get_not_owner_response()
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class MenuModelListAPIView(ListAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuModelListSerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class MyMenuList(ListAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuModelListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        current_set = self.queryset
        params = self.request.query_params
        branch_id = params.get('branch', None)
        status = params.get('status', None)

        if status is not None:
            current_set = current_set.filter(status=status)

        if branch_id is not None:
            current_set = current_set.filter(branch_id=branch_id)

        if self.request.user.parent is None:
            return current_set.filter(branch__company__user_id=self.request.user.id)
        else:
            return current_set.filter(branch__user_id=self.request.user.id)


class MenuModelDetailAPIView(RetrieveAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuModelDetailSerializer

    def get(self, request, *args, **kwargs):
        menu = Menu.objects.filter(id=kwargs["pk"]).first()
        if menu is None:
            return get_not_found_response()
        return get_detail_successfully_response(MenuModelDetailSerializer(menu, context={'request': request}).data)


class MenuModelUpdateAPIView(RetrieveUpdateAPIView):
    not_null_field = ["title"]
    queryset = Menu.objects.all()
    serializer_class = MenuModelUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            menu = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, menu):
            return get_not_owner_response()

        image = self.get_object().image
        if bool(self.get_object().image) and os.path.isfile(image.path):
            os.remove(image.path)

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

    @check_auth
    def patch(self, request, *args, **kwargs):
        try:
            menu = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, menu):
            return get_not_owner_response()

        data = request.data
        if menu:
            mode = data.get("mode")
            if mode is not None:

                if mode == "change-category-sort-order":
                    newOrders = request.data["newOrders"]
                    category_list = Category.objects.filter(menu_id=menu.id)
                    if newOrders is not None:
                        if len(category_list) == len(newOrders):
                            for category in category_list:
                                category.order = newOrders[str(category.id)]
                                category.save()
                        else:
                            return get_bad_request_message_response("Kategoriler ile sıra dizisi uyuşmamaktadır.")
                    else:
                        return get_bad_request_key_response("newOrders")

                elif mode == "update-status":
                    status = data.get("status")
                    if status is not None:
                        menu.status = status
                        menu.save()
                    else:
                        return get_bad_request_key_response("status")

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class MenuModelDeleteAPIView(DestroyAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuModelDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            menu = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, menu):
            return get_not_owner_response()

        if bool(menu.image) and os.path.isfile(menu.image.path):
            os.remove(menu.image.path)

        menu.delete()

        return get_deleted_successfully_response()
