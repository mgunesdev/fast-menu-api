# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.category.serializers import CategoryDetailSerializer
from apps.helper.Base64ImageField import Base64ImageField
from apps.helper.responseProcess.ModelResponseHelper import *
from app.models import Menu, Category, Product, Branch, QrCode
from apps.product.serializers import ProductListSerializer
from apps.qrcode.serializers import QrCodeSerializer, QrCodeMenuListSerializer


class MenuModelCreateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Menu
        fields = [
            'id',
            'branch',
            'title',
            'image',
            'status'
        ]


class MenuModelListSerializer(ModelSerializer):
    category_count = SerializerMethodField()
    product_count = SerializerMethodField()
    branch = SerializerMethodField()
    qr_code = SerializerMethodField()

    class Meta:
        model = Menu
        fields = [
            'id',
            'branch',
            'title',
            'image',
            'category_count',
            'product_count',
            'qr_code',
            'status'
        ]

    def get_branch(self, obj):
        if obj.branch is None:
            return None
        return get_short_branch_model(self, obj.branch)

    def get_category_count(self, obj):
        c_qs = Category.objects.filter(menu_id=obj.id)
        return len(c_qs)

    def get_product_count(self, obj):
        c_qs = Product.objects.filter(menu_id=obj.id)
        return len(c_qs)

    def get_qr_code(self, obj):
        try:
            code = QrCode.objects.get(menu_id=obj.id)
            if code:
                data = QrCodeMenuListSerializer(code).data
                return data
        except Exception:
            pass

        return None


class MenuModelDetailSerializer(ModelSerializer):
    branch = SerializerMethodField()
    category_count = SerializerMethodField()
    product_count = SerializerMethodField()
    categories = SerializerMethodField()
    qr_code = SerializerMethodField()
    trends = SerializerMethodField()
    should_tastes = SerializerMethodField()

    # products = SerializerMethodField()

    class Meta:
        model = Menu
        fields = [
            'id',
            'branch',
            'title',
            'is_primary',
            'image',
            'category_count',
            'product_count',
            'categories',
            "qr_code",
            'status',
            'trends',
            'should_tastes'
        ]

    def get_branch(self, obj):
        if obj.branch is None:
            return None
        return get_short_branch_model(self, obj.branch)

    def get_categories(self, obj):
        c_qs = Category.objects.filter(menu_id=obj.id)
        categories = CategoryDetailSerializer(c_qs, many=True, context={'request': self.context['request']}).data
        return categories

    # def get_products(self, obj):
    #     c_qs = Product.objects.filter(menu_id=obj.id)
    #     products = ProductDetailSerializer(c_qs, many=True).data
    #     return products

    def get_category_count(self, obj):
        c_qs = Category.objects.filter(menu_id=obj.id)
        return len(c_qs)

    def get_product_count(self, obj):
        c_qs = Product.objects.filter(menu_id=obj.id)
        return len(c_qs)

    def get_qr_code(self, obj):
        try:
            code = QrCode.objects.get(menu_id=obj.id)
            if code:
                data = QrCodeSerializer(code).data
                data.pop("menu")
                return data
        except Exception:
            pass

        return None

    def get_trends(self, obj):
        c_qs = Product.objects.filter(menu_id=obj.id, is_trend=True)
        return ProductListSerializer(c_qs, many=True, context={'request': self.context['request']}).data

    def get_should_tastes(self, obj):
        c_qs = Product.objects.filter(menu_id=obj.id, is_should_taste=True)
        return ProductListSerializer(c_qs, many=True, context={'request': self.context['request']}).data


class MenuModelShortListSerializer(ModelSerializer):
    class Meta:
        model = Menu
        fields = [
            'id',
            'title',
            'image'
        ]


class MenuModelUpdateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True, allow_null=True, required=False
    )

    class Meta:
        model = Menu
        fields = [
            'id',
            'title',
            'image',
            'is_primary',
            'status'
        ]


class MenuModelDeleteSerializer(ModelSerializer):
    class Meta:
        model = Menu
        fields = ['id']
