# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AnonymousUser
from django.db.models import Avg
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.branch.branchImage.serializers import BranchImageSerializer
from apps.branch.comment.serializers import BranchCommentDetailSerializer
from apps.helper.responseProcess.ModelResponseHelper import *
from app.models import Branch, BranchProperty, BranchComment, BranchFollow, BranchShare, Menu
from apps.helper.serializers.dynamic_fields import DynamicFieldsSerializerMixin
from apps.location.serializers import LocationSerializer
from apps.menu.serializers import MenuModelShortListSerializer
from apps.property.serializers import PropertySerializer
import json


class BranchCreateSerializer(ModelSerializer):
    class Meta:
        model = Branch
        fields = [
            'id',
            'user',
            'company',
            'name',
            'location',
            'working_time',
            'status'

        ]


class BranchListSerializer(DynamicFieldsSerializerMixin, ModelSerializer):
    images = BranchImageSerializer(source="branch_image", many=True, read_only=True)
    user = SerializerMethodField()
    company = SerializerMethodField()
    location = LocationSerializer()
    properties = SerializerMethodField()
    average_rate = SerializerMethodField()
    menu_count = SerializerMethodField()
    working_time = SerializerMethodField()
    phones = BranchPhoneNumberSerializer(source="phone_number_branch", many=True, read_only=True)

    class Meta:
        model = Branch
        fields = ['id',
                  'user',
                  'company',
                  'name',
                  'images',
                  'properties',
                  "average_rate",
                  'location',
                  'status',
                  'menu_count',
                  'working_time',
                  'phones'
                  ]

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)

    def get_company(self, obj):
        request = self.context.get('request')
        if request:
            return get_short_company_model(self, obj.company)
        else:
            return obj.company.id

    def get_properties(self, obj):
        c_qs = BranchProperty.objects.filter(branch_id=obj.id)
        if c_qs and len(c_qs) > 0:
            property_list = []
            for branch_property in c_qs:
                property_list.append(PropertySerializer(branch_property.property).data)
            return property_list
        else:
            return None

    def get_average_rate(self, obj):
        rate = BranchComment.objects.filter(branch_id=obj.id, parent=None).aggregate(Avg('rate')).get("rate__avg")
        if rate is not None:
            return round(
                rate, 1)

        return 0

    def get_menu_count(self, obj):
        return Menu.objects.filter(branch_id=obj.id).count()

    def get_working_time(self, obj):
        working_json = {}
        summary_json = {}
        resutl_summary = []
        try:
            days = json.loads(obj.working_time)
            working_json["days"] = days

            for day, value in days.items():
                if value not in summary_json:
                    summary_json[value] = []

                summary_json[value].append(day)

            for time, days in summary_json.items():
                resutl_summary.append("{}/{}".format(time, ",".join(days)))

            working_json["summary"] = resutl_summary
            return working_json
        except Exception:
            return None


class BranchDetailSerializer(ModelSerializer):
    images = BranchImageSerializer(source="branch_image", many=True, read_only=True)
    phones = BranchPhoneNumberSerializer(source="phone_number_branch", many=True, read_only=True)
    properties = SerializerMethodField()
    location = LocationSerializer()
    comments = SerializerMethodField()
    comment_count = SerializerMethodField()
    average_rate = SerializerMethodField()
    follow_count = SerializerMethodField()
    bookmark = SerializerMethodField()
    share_count = SerializerMethodField()
    user = SerializerMethodField()
    company = SerializerMethodField()
    menus = SerializerMethodField()
    working_time = SerializerMethodField()

    class Meta:
        model = Branch
        fields = ['id',
                  'user',
                  'company',
                  'name',
                  'images',
                  'phones',
                  "comments",
                  "comment_count",
                  "average_rate",
                  'properties',
                  'follow_count',
                  'bookmark',
                  'share_count',
                  'location',
                  'menus',
                  'working_time',
                  'status'

                  ]

    def get_properties(self, obj):
        c_qs = BranchProperty.objects.filter(branch_id=obj.id)
        if c_qs and len(c_qs) > 0:
            property_list = []
            for branch_property in c_qs:
                property_list.append(PropertySerializer(branch_property.property).data)
            return property_list
        else:
            return None

    def get_comments(self, obj):
        c_qs = BranchComment.objects.filter(branch_id=obj.id, parent=None)[:2]
        if c_qs.count() == 0:
            return None
        comments = BranchCommentDetailSerializer(c_qs, many=True).data
        return comments

    def get_comment_count(self, obj):
        return BranchComment.objects.filter(branch_id=obj.id).count()

    def get_average_rate(self, obj):
        c_qs = BranchComment.objects.filter(branch_id=obj.id, parent=None)
        count = len(c_qs)
        if count == 0:
            return 0
        rate = 0
        for comment in c_qs:
            rate += comment.rate
        return round(rate / count, 1)

    def get_follow_count(self, obj):
        return BranchFollow.objects.filter(branch_id=obj.id).count()

    def get_bookmark(self, obj):
        if type(self.context['request'].user) is not AnonymousUser:
            try:
                bookmark = BranchFollow.objects.get(branch_id=obj.id, user_id=self.context['request'].user.id)
                return {"id": bookmark.id}
            except Exception:
                pass

            return None

    def get_share_count(self, obj):
        return BranchShare.objects.filter(branch_id=obj.id).count()

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)

    def get_company(self, obj):
        request = self.context.get('request')
        if request:
            return get_short_company_model(self, obj.company)
        else:
            return obj.company.id

    def get_menus(self, obj):
        query = Menu.objects.filter(branch_id=obj.id, status=1)
        if query.count() != 0:
            return MenuModelShortListSerializer(query, many=True).data

        return None

    def get_working_time(self, obj):

        working_json = {}
        summary_json = {}
        resutl_summary = []
        days = json.loads(obj.working_time)

        working_json["days"] = days

        for day, value in days.items():
            if value not in summary_json:
                summary_json[value] = []

            summary_json[value].append(day)

        for time, days in summary_json.items():
            resutl_summary.append("{}/{}".format(time, ",".join(days)))

        working_json["summary"] = resutl_summary

        return working_json


class BranchUpdateSerializer(ModelSerializer):
    class Meta:
        model = Branch
        fields = ['id',
                  'name',
                  'location',
                  'working_time',
                  'status'
                  ]


class BranchDeleteSerailizer(ModelSerializer):
    class Meta:
        model = Branch
        fields = ['id']
