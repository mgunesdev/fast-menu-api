# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.urls import include

from apps.branch.views import (
    BranchCreateAPIView,
    BranchListAPIView,
    BranchDeleteAPIView,
    BranchUpdateAPIView,
    BranchDetailAPIView,
    MyBranchListAPIView

)

urlpatterns = [
    url(r'^create/$', BranchCreateAPIView.as_view(), name='api-branch-create'),
    url(r'^detail/(?P<pk>\d+)', BranchDetailAPIView.as_view(), name='api-branch-detail'),
    url(r'^delete/(?P<pk>\d+)', BranchDeleteAPIView.as_view(), name='api-branch-delete'),
    url(r'^list/$', BranchListAPIView.as_view(), name='api-branch-list'),
    url(r'^my-branch/$', MyBranchListAPIView.as_view(), name='api-my-branch-list'),
    url(r'^edit/(?P<pk>\d+)', BranchUpdateAPIView.as_view(), name='api-branch-edit'),
    url(r'^image/', include('apps.branch.branchImage.urls')),
    url(r'^phones/', include('apps.branch.branchPhoneNumber.urls')),
    url(r'^follow/', include('apps.branch.branchFollow.urls')),
    url(r'^comment/', include('apps.branch.comment.urls')),
    url(r'^share/', include('apps.branch.branchShare.urls'))

]
