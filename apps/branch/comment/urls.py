# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.branch.comment.views import (
    BranchCommentListAPIView,
    BranchCommentDetailAPIView,
    BranchCommentCreateAPIView,
    BranchCommentDeleteAPIView,
    BranchCommentUpdateAPIView
)

urlpatterns = [
    url(r'^list/$', BranchCommentListAPIView.as_view(), name='api-branch-comment-list'),
    url(r'^detail/(?P<pk>\d+)', BranchCommentDetailAPIView.as_view(), name='api-branch-comment-detail'),
    url(r'^create/$', BranchCommentCreateAPIView.as_view(), name='api-branch-comment-create'),
    url(r'^delete/(?P<pk>\d+)', BranchCommentDeleteAPIView.as_view(), name='api-branch-comment-delete'),
    url(r'^edit/(?P<pk>\d+)', BranchCommentUpdateAPIView.as_view(), name='api-branch-comment-edit')

]
