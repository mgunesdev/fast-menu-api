# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from apps.helper.responseProcess.ModelResponseHelper import get_short_user_model

from app.models import BranchComment


class BranchCommentSerializer(ModelSerializer):
    reply_count = SerializerMethodField()

    class Meta:
        model = BranchComment
        fields = ['id',
                  'comment',
                  'rate',
                  'branch',
                  'user',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0


class BranchListCommentSerializer(ModelSerializer):
    reply_count = SerializerMethodField()
    user = SerializerMethodField()

    class Meta:
        model = BranchComment
        fields = ['id',
                  'comment',
                  'rate',
                  'branch',
                  'user',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class BranchCommentChildSerializer(ModelSerializer):
    class Meta:
        model = BranchComment
        fields = [
            'id',
            'comment',
            'comment_create_date',
        ]


class BranchCommentDetailSerializer(ModelSerializer):
    replies = SerializerMethodField()
    reply_count = SerializerMethodField()
    user = SerializerMethodField()

    class Meta:
        model = BranchComment
        fields = ['id',
                  'comment',
                  'branch',
                  'user',
                  'rate',
                  'replies',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return None

    def get_replies(self, obj):
        if obj.is_parent:
            return BranchCommentChildSerializer(obj.children(), many=True).data
        return None

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class BranchCommentDeleteSerializer(ModelSerializer):
    class Meta:
        model = BranchComment
        fields = [
            'id'
        ]


class BranchCommentUpdateSerializer(ModelSerializer):
    class Meta:
        model = BranchComment
        fields = ['comment',
                  'rate']
