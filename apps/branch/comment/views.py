# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView
)

from app.models import BranchComment, Branch
from .serializers import (
    BranchCommentSerializer,
    BranchCommentDetailSerializer,
    BranchCommentDeleteSerializer,
    BranchCommentUpdateSerializer,
    BranchListCommentSerializer
)


class BranchCommentDetailAPIView(RetrieveAPIView):
    queryset = BranchComment.objects.all()
    serializer_class = BranchCommentDetailSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        comment = BranchComment.objects.filter(id=kwargs["pk"]).first()
        if comment is None:
            return get_not_found_response()
        return get_detail_successfully_response(BranchCommentDetailSerializer(comment).data)


class BranchCommentCreateAPIView(CreateAPIView):
    queryset = BranchComment.objects.all()
    serializer_class = BranchCommentSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class BranchCommentListAPIView(ListAPIView):
    queryset = BranchComment.objects.all()
    serializer_class = BranchListCommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['branch']

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class BranchCommentDeleteAPIView(DestroyAPIView):
    queryset = BranchComment.objects.all()
    serializer_class = BranchCommentDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            comment = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, comment):
            return get_not_owner_response()

        comment.delete()

        return get_deleted_successfully_response()


class BranchCommentUpdateAPIView(RetrieveUpdateAPIView):
    queryset = BranchComment.objects.all()
    serializer_class = BranchCommentUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            comment = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, comment):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
