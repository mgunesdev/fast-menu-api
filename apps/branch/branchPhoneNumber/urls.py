# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.branch.branchPhoneNumber.views import (
    BranchPhoneNumberCreateAPIView,
    BranchPhoneNumberListAPIView,
    BranchPhoneNumberDeleteAPIView,
    BranchPhoneNumberUpdateAPIView
)

urlpatterns = [
    url(r'^list/$', BranchPhoneNumberListAPIView.as_view(), name='api-branch-phone-number-list'),
    url(r'^delete/(?P<pk>\d+)', BranchPhoneNumberDeleteAPIView.as_view(), name='api-branch-phone-number-delete'),
    url(r'^create/$', BranchPhoneNumberCreateAPIView.as_view(), name='api-branch-phone-number-create'),
    url(r'^edit/(?P<pk>\d+)', BranchPhoneNumberUpdateAPIView.as_view(), name='api-branch-phone-number-edit')
]
