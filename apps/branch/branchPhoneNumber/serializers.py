# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer
from app.models import BranchPhoneNumber


class BranchPhoneNumberCreateSerializer(ModelSerializer):
    class Meta:
        model = BranchPhoneNumber
        fields = [
            'id',
            'branch',
            'country_code',
            'phone'
        ]


class BranchPhoneNumberSerializer(ModelSerializer):
    class Meta:
        model = BranchPhoneNumber
        fields = [
            'id',
            'branch',
            'country_code',
            'phone'
        ]

class BranchPhoneNumberUpdateSerializer(ModelSerializer):
    class Meta:
        model = BranchPhoneNumber
        fields = [
            'id',
            'country_code',
            'phone'
        ]


class BranchPhoneNumberDeleteSerializer(ModelSerializer):
    class Meta:
        model = BranchPhoneNumber
        fields = ['id']
