# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.exceptions import ValidationError
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.decorators import (
    check_auth,
    check_branch_availability
)

from apps.helper.responseProcess.ResponseHelper import *
from app.models import BranchPhoneNumber
from .serializers import (
    BranchPhoneNumberCreateSerializer,
    BranchPhoneNumberSerializer,
    BranchPhoneNumberUpdateSerializer,
    BranchPhoneNumberDeleteSerializer
)


class BranchPhoneNumberCreateAPIView(CreateAPIView):
    queryset = BranchPhoneNumber.objects.all()
    serializer_class = BranchPhoneNumberCreateSerializer

    @check_auth
    @check_branch_availability
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class BranchPhoneNumberListAPIView(ListAPIView):
    queryset = BranchPhoneNumber.objects.all()
    serializer_class = BranchPhoneNumberSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class BranchPhoneNumberDeleteAPIView(DestroyAPIView):
    queryset = BranchPhoneNumber.objects.all()
    serializer_class = BranchPhoneNumberDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            number = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, number):
            return get_not_owner_response()

        number.delete()

        return get_deleted_successfully_response()


class BranchPhoneNumberUpdateAPIView(RetrieveUpdateAPIView):
    queryset = BranchPhoneNumber.objects.all()
    serializer_class = BranchPhoneNumberUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            number = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, number):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
