# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

from django.db.models import Q, Avg
from django.contrib.gis.measure import D
from django.contrib.gis.geos import Point

from app.decorators import check_auth, check_company_availability
from apps.helper.polygonChecker import get_city_district_from_point

from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from apps.helper.ApiViewHelper import checkDataGetSerializer
from apps.helper.responseProcess.ResponseHelper import *
from app.models import Branch, BranchImage, BranchPhoneNumber, User, Company, Location, Property, BranchProperty
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin

from .serializers import (
    BranchCreateSerializer,
    BranchListSerializer,
    BranchDetailSerializer,
    BranchUpdateSerializer,
    BranchDeleteSerailizer
)


class BranchCreateAPIView(CreateAPIView):
    queryset = Branch
    serializer_class = BranchCreateSerializer

    @check_auth
    @check_company_availability
    def post(self, request, *args, **kwargs):
        data = request.data

        try:
            serializer = checkDataGetSerializer(self, data)
            headers = self.get_success_headers(serializer.data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        name = data["name"]
        user = User.objects.get(id=data["user"])
        company = Company.objects.get(id=data["company"])

        new_branch = Branch()
        new_branch.user = user
        new_branch.company = company
        new_branch.name = name
        location = data.get("location")
        if location:
            try:
                model = Location.objects.get(id=location)
                new_branch.location = model

            except Exception:
                pass

        working_time = data.get("working_time")
        print("Working Time", working_time)
        if working_time:
            print("Working İç")
            new_branch.working_time = working_time
        new_branch.save()

        phones = data.get("phones")
        if phones and len(phones) > 0:
            for current_phone in phones:
                new_branch_phone = BranchPhoneNumber()
                new_branch_phone.branch = new_branch
                new_branch_phone.phone = current_phone["phone"]
                new_branch_phone.country_code = current_phone["country_code"]
                new_branch_phone.user = user
                new_branch_phone.save()

        property_id_list = data.get("properties")
        if property_id_list and len(property_id_list) > 0:
            for property_id in property_id_list:
                selected_property = Property.objects.get(id=property_id)
                new_branch_property = BranchProperty()
                new_branch_property.branch = new_branch
                new_branch_property.property = selected_property
                new_branch_property.save()

        return get_created_successfully_response(BranchCreateSerializer(new_branch).data)


class BranchListAPIView(DynamicFieldsViewMixin, ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        property = self.request.query_params.get('property', None)
        point = self.request.query_params.get("point", None)
        order = self.request.query_params.get("order", None)
        sort = self.request.query_params.get("sort", None)
        queryset = Branch.objects.all()
        if property is not None:
            queryset = queryset.filter(property_branch__property__code=property)
        if point is not None and "," in point and sort is None:
            city, district, locality = get_city_district_from_point(point)
            if district is not None:
                queryset = queryset.filter(location__locality__parentcode=district.code)
        if order is not None:
            if sort == "rate":
                if point is not None:
                    city, district, locality = get_city_district_from_point(point)
                    queryset = queryset.filter(location__city=city.code)
                order_key = "avg_rating"
                if order == "desc":
                    order_key = "-{}".format(order_key)
                queryset = queryset.annotate(avg_rating=Avg('branch_comment_branch__rate')).filter(
                    avg_rating__gte=2.5).order_by(order_key)
            if sort == "location":
                order_key = "location__point"
                if order == "desc":
                    order_key = "-{}".format(order_key)
                if point is not None and "," in point:
                    point_array = point.split(",")
                    lat = float(point_array[0])
                    lng = float(point_array[1])
                    new_point = Point(lat, lng)
                    radius = 25

                    queryset = queryset.filter(location__point__distance_lte=(new_point, D(km=radius))).order_by(
                        order_key)
        return queryset


class MyBranchListAPIView(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        current_set = self.queryset
        params = self.request.query_params
        status = params.get('status', None)

        if status is not None:
            current_set = current_set.filter(status=status)

        if self.request.user.parent is None:
            return current_set.filter(company__user_id=self.request.user.id)
        else:
            return current_set.filter(user_id=self.request.user.id)


class BranchDetailAPIView(RetrieveAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchDetailSerializer

    def get(self, request, *args, **kwargs):
        branch = Branch.objects.filter(id=kwargs["pk"]).first()
        if branch is None:
            return get_not_found_response()
        return get_detail_successfully_response(BranchDetailSerializer(branch, context={'request': request}).data)


class BranchUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        data = request.data
        try:
            current_branch = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, current_branch):
            return get_not_owner_response()

        try:
            serializer = checkDataGetSerializer(self, data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        name = data["name"]
        current_branch.name = name
        location = Location.objects.get(id=data["location"])
        current_branch.location = location

        branch_property_list = BranchProperty.objects.filter(branch_id=current_branch.id)
        property_id_list = data.get("properties")

        if len(branch_property_list) == 0:
            if property_id_list and len(property_id_list) > 0:
                for property_id in property_id_list:
                    try:
                        selected_property = Property.objects.get(id=property_id)
                        new_branch_property = BranchProperty()
                        new_branch_property.branch = current_branch
                        new_branch_property.property = selected_property
                        new_branch_property.save()
                    except Exception as exp:
                        pass

        elif property_id_list is not None and len(property_id_list) == 0:

            for current_branch_property in branch_property_list:
                current_branch_property.delete()
        else:
            if property_id_list:
                for current_branch_property in branch_property_list:
                    if not current_branch_property.property.id in property_id_list:
                        current_branch_property.delete()
                    else:
                        property_id_list.remove(current_branch_property.property.id)

                for property_id in property_id_list:
                    try:
                        selected_property = Property.objects.get(id=property_id)
                        new_branch_property = BranchProperty()
                        new_branch_property.branch = current_branch
                        new_branch_property.property = selected_property
                        new_branch_property.save()
                    except Exception as exp:
                        pass

        current_branch_phones = BranchPhoneNumber.objects.filter(branch_id=current_branch.id)
        phones = data.get("phones")

        if len(current_branch_phones) == 0:
            phones = data.get("phones")
            if phones and len(phones) > 0:
                for current_phone in phones:
                    new_branch_phone = BranchPhoneNumber()
                    new_branch_phone.branch = current_branch
                    new_branch_phone.phone = current_phone["phone"]
                    new_branch_phone.country_code = current_phone["country_code"]
                    new_branch_phone.user = current_branch.user
                    new_branch_phone.save()
        elif phones and len(phones) == 0:
            for current_branch_phone in current_branch_phones:
                current_branch_phone.delete()
        else:

            if phones:
                for phone in phones:

                    phone_id = phone.get("id")
                    number = phone.get("phone")
                    country_code = phone.get("country_code")

                    if phone_id:
                        updated_phone_number = BranchPhoneNumber.objects.get(id=phone_id)
                        updated_phone_number.phone = number
                        updated_phone_number.country_code = country_code
                        updated_phone_number.save()
                    else:
                        new_branch_phone = BranchPhoneNumber()
                        new_branch_phone.branch = current_branch
                        new_branch_phone.phone = number
                        new_branch_phone.country_code = country_code
                        new_branch_phone.user = current_branch.user
                        new_branch_phone.save()

                for current_branch_phone in current_branch_phones:
                    is_contains_new_list = False
                    for phone in phones:
                        if current_branch_phone.id == phone.get("id"):
                            is_contains_new_list = True

                    if not is_contains_new_list:
                        current_branch_phone.delete()

        working_time = data.get("working_time")
        if working_time:
            current_branch.working_time = working_time
        current_branch.save()

        return get_updated_successfully_response(BranchUpdateSerializer(current_branch).data)

    @check_auth
    def patch(self, request, *args, **kwargs):
        try:
            branch = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, branch):
            return get_not_owner_response()

        try:
            mode = request.data["mode"]
        except Exception:
            return get_bad_request_key_response("mode")

        data = request.data

        if mode == "change-orders":
            new_orders = request.data["new-orders"]
            for id in new_orders:
                int_id = int(id)
                order = new_orders[id]
                branch_image = BranchImage.objects.filter(id=int_id).first()
                branch_image.order = order
                branch_image.save()

        elif mode == "delete-image":
            image_id = request.data["image-id"]
            image_list = list(branch.branch_image.all())
            del_image = BranchImage.objects.filter(id=image_id)
            del_image = del_image.first()
            index = image_list.index(del_image)
            image_list.remove(del_image)

            for i in range(index, len(image_list)):
                branch_image = image_list[i]
                if branch_image.order == 1:
                    break
                branch_image.order = branch_image.order - 1
                branch_image.save()

            del_image.delete()

            branch.save()
        elif mode == "update-status":
            status = data.get("status")
            if status is not None:
                branch.status = status
                branch.save()
            else:
                return get_bad_request_key_response("status")

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class BranchDeleteAPIView(DestroyAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchDeleteSerailizer

    def destroy(self, request, *args, **kwargs):
        try:
            branch = self.get_object()
        except Exception:
            return get_not_found_response()

        if not is_authenticated(request):
            return get_not_authenticated_response()

        if not check_owner_for_model(request, branch):
            return get_not_owner_response()

        for branch_image in BranchImage.objects.filter(branch_id=branch.id):
            if os.path.isfile(branch_image.image.path):
                os.remove(branch_image.image.path)

        branch.delete()
        return get_deleted_successfully_response()
