# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.branch.branchShare.views import (
    BranchShareCreateAPIView
)

urlpatterns = [
    url(r'^create/$', BranchShareCreateAPIView.as_view(), name='api-branch-share-create')
]
