# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer

from app.models import BranchShare


class BranchShareSerializer(ModelSerializer):
    class Meta:
        model = BranchShare
        fields = [
            'id',
            'user',
            'branch',
            'share_date'
        ]
