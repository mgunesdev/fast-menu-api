# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.branch.branchFollow.views import (
    BranchFollowCreateAPIView,
    BranchFollowDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', BranchFollowCreateAPIView.as_view(), name='api-branch-follow-create'),
    url(r'^delete/(?P<pk>\d+)', BranchFollowDeleteAPIView.as_view(), name='api-branch-follow-delete')

]
