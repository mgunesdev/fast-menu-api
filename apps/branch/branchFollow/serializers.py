# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer

from app.models import BranchFollow


class BranchFollowSerializer(ModelSerializer):
    class Meta:
        model = BranchFollow
        fields = [
            'id',
            'user',
            'branch',
            'created_at'
        ]


class BranchFollowDeleteSerializer(ModelSerializer):
    class Meta:
        model = BranchFollow
        fields = ["id"]
