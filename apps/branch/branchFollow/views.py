# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
)

from app.decorators import check_auth
from app.models import BranchFollow
from apps.helper.responseProcess.ResponseHelper import *
from .serializers import (
    BranchFollowSerializer,
    BranchFollowDeleteSerializer
)


class BranchFollowCreateAPIView(CreateAPIView):
    queryset = BranchFollow.objects.all()
    serializer_class = BranchFollowSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        c_qs = BranchFollow.objects.filter(branch_id=request.data.get("branch"), user_id=request.data.get("user"))

        try:
            model = BranchFollow.objects.get(branch_id=request.data.get("branch"), user_id=request.data.get("user"))
            return get_already_done_response(BranchFollowSerializer(model).data)
        except Exception:
            pass

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class BranchFollowDeleteAPIView(DestroyAPIView):
    queryset = BranchFollow.objects.all()
    serializer_class = BranchFollowDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            branch_follow = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, branch_follow):
            return get_not_owner_response()

        branch_follow.delete()

        return get_deleted_successfully_response()
