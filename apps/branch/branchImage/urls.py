# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.branch.branchImage.views import (
    BranchImageCreateAPIView,
    BranchImageListAPIView,
    BranchImageDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', BranchImageCreateAPIView.as_view(), name='api-branch-image-create'),
    url(r'^list/$', BranchImageListAPIView.as_view(), name='api-branch-image-list'),
    url(r'^delete/(?P<pk>\d+)', BranchImageDeleteAPIView.as_view(), name='api-branch-image-delete')
]
