# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from apps.helper.Base64ImageField import Base64ImageField
from app.models import BranchImage


class BranchImageCreateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, allow_empty_file=True, use_url=True,
    )

    class Meta:
        model = BranchImage
        fields = ['id', 'branch', 'image']


class BranchImageSerializer(ModelSerializer):
    class Meta:
        model = BranchImage
        fields = ['id', 'image', 'order']


class BranchImageDeleteSerializer(ModelSerializer):
    class Meta:
        model = BranchImage
        fields = ['id']
