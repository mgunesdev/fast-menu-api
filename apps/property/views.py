# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.models import Property
from .serializers import (
    PropertySerializer
)


class PropertyCreateAPIView(CreateAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PropertyListAPIView(ListAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class PropertyDetailAPIView(RetrieveAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Property.objects.filter(id=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class PropertyUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PropertyDeleteAPIView(DestroyAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        self.get_object().delete()

        return get_deleted_successfully_response()
