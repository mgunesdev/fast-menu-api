# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Property


class PropertySerializer(ModelSerializer):
    class Meta:
        model = Property
        fields = '__all__'
