# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.property.views import (
    PropertyCreateAPIView,
    PropertyListAPIView,
    PropertyDetailAPIView,
    PropertyDeleteAPIView,
    PropertyUpdateAPIView
)

urlpatterns = [
    url(r'^create/$', PropertyCreateAPIView.as_view(), name='api-property-create'),
    url(r'^detail/(?P<pk>\d+)', PropertyDetailAPIView.as_view(), name='api-property-detail'),
    url(r'^delete/(?P<pk>\d+)', PropertyDeleteAPIView.as_view(), name='api-property-delete'),
    url(r'^list/$', PropertyListAPIView.as_view(), name='api-property-list'),
    url(r'^edit/(?P<pk>\d+)', PropertyUpdateAPIView.as_view(), name='api-product-comment-edit')

]
