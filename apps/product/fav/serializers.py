# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer

from app.models import ProductFav


class ProductFavSerializer(ModelSerializer):
    class Meta:
        model = ProductFav
        fields = [
            'id',
            'user',
            'product',
            'fav_date'
        ]


class ProductFavDeleteSerializer(ModelSerializer):
    class Meta:
        model = ProductFav
        fields = ["id"]
