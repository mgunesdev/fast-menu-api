# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.fav.views import (
    ProductFavCreateAPIView,
    ProductFavDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', ProductFavCreateAPIView.as_view(), name='api-product-fav-create'),
    url(r'^delete/(?P<pk>\d+)', ProductFavDeleteAPIView.as_view(), name='api-product-fav-delete')

]
