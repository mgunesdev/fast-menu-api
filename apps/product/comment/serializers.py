# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from apps.helper.responseProcess.ModelResponseHelper import get_short_user_model


from app.models import ProductComment


class ProductCommentSerializer(ModelSerializer):
    reply_count = SerializerMethodField()

    class Meta:
        model = ProductComment
        fields = ['id',
                  'comment',
                  'rate',
                  'product',
                  'user',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0


class ProductCommentListSerializer(ModelSerializer):
    reply_count = SerializerMethodField()
    user = SerializerMethodField()


    class Meta:
        model = ProductComment
        fields = ['id',
                  'comment',
                  'rate',
                  'product',
                  'user',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class ProductCommentChildSerializer(ModelSerializer):
    class Meta:
        model = ProductComment
        fields = [
            'id',
            'comment',
            'comment_create_date',
        ]


class ProductCommentDetailSerializer(ModelSerializer):
    replies = SerializerMethodField()
    reply_count = SerializerMethodField()
    user = SerializerMethodField()


    class Meta:
        model = ProductComment
        fields = ['id',
                  'comment',
                  'product',
                  'user',
                  'rate',
                  'replies',
                  'reply_count',
                  'comment_create_date',
                  'parent']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return None

    def get_replies(self, obj):
        if obj.is_parent:
            return ProductCommentChildSerializer(obj.children(), many=True).data
        return None

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class ProductCommentDeleteSerializer(ModelSerializer):
    class Meta:
        model = ProductComment
        fields = [
            'id'
        ]


class ProductCommentUpdateSerializer(ModelSerializer):
    class Meta:
        model = ProductComment
        fields = ['comment',
                  'rate']
