# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView
)

from app.models import ProductComment, Product
from .serializers import (
    ProductCommentSerializer,
    ProductCommentDetailSerializer,
    ProductCommentDeleteSerializer,
    ProductCommentUpdateSerializer,
    ProductCommentListSerializer
)


class ProductCommentDetailAPIView(RetrieveAPIView):
    queryset = ProductComment.objects.all()
    serializer_class = ProductCommentDetailSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        comment = ProductComment.objects.filter(id=kwargs["pk"]).first()
        if comment is None:
            return get_not_found_response()
        return get_detail_successfully_response(ProductCommentDetailSerializer(comment).data)


class ProductCommentCreateAPIView(CreateAPIView):
    queryset = ProductComment.objects.all()
    serializer_class = ProductCommentSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class ProductCommentListAPIView(ListAPIView):
    queryset = ProductComment.objects.all()
    serializer_class = ProductCommentListSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['product']

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class ProductCommentDeleteAPIView(DestroyAPIView):
    queryset = ProductComment.objects.all()
    serializer_class = ProductCommentDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            comment = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, comment):
            return get_not_owner_response()

        comment.delete()

        return get_deleted_successfully_response()


class ProductCommentUpdateAPIView(RetrieveUpdateAPIView):
    queryset = ProductComment.objects.all()
    serializer_class = ProductCommentUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            comment = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, comment):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
