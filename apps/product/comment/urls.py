# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.comment.views import (
    ProductCommentListAPIView,
    ProductCommentDetailAPIView,
    ProductCommentCreateAPIView,
    ProductCommentDeleteAPIView,
    ProductCommentUpdateAPIView
)

urlpatterns = [
    url(r'^list/$', ProductCommentListAPIView.as_view(), name='api-product-comment-list'),
    url(r'^detail/(?P<pk>\d+)', ProductCommentDetailAPIView.as_view(), name='api-product-comment-detail'),
    url(r'^create/$', ProductCommentCreateAPIView.as_view(), name='api-product-comment-create'),
    url(r'^delete/(?P<pk>\d+)', ProductCommentDeleteAPIView.as_view(), name='api-product-comment-delete'),
    url(r'^edit/(?P<pk>\d+)', ProductCommentUpdateAPIView.as_view(), name='api-product-comment-edit')

]
