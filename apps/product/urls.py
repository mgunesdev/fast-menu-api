# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.urls import include

from apps.product.views import (
    ProductCreateAPIView,
    ProductListAPIView,
    ProductDetailAPIView,
    ProductDeleteAPIView,
    ProductUpdateAPIView,
    MyProductListAPIView
)

urlpatterns = [
    url(r'^create/$', ProductCreateAPIView.as_view(), name='api-product-create'),
    url(r'^detail/(?P<pk>\d+)', ProductDetailAPIView.as_view(), name='api-product-detail'),
    url(r'^delete/(?P<pk>\d+)', ProductDeleteAPIView.as_view(), name='api-product-delete'),
    url(r'^list/$', ProductListAPIView.as_view(), name='api-product-list'),
    url(r'^my-product/$', MyProductListAPIView.as_view(), name='api-my-product-list'),
    url(r'^edit/(?P<pk>\d+)', ProductUpdateAPIView.as_view(), name='api-product-edit'),
    url(r'^image/', include('apps.product.productImage.urls')),
    url(r'^ingredient/', include('apps.product.productIngredients.urls')),
    url(r'^price/', include('apps.product.price.urls')),
    url(r'^time/', include('apps.product.preparingTime.urls')),
    url(r'^comment/', include('apps.product.comment.urls')),
    url(r'^fav/', include('apps.product.fav.urls')),
    url(r'^share/', include('apps.product.share.urls'))

]
