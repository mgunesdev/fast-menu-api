# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.productImage.views import (
    ProductImageCreateAPIView,
    ProductImageListAPIView,
    ProductImageDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', ProductImageCreateAPIView.as_view(), name='api-product-image-create'),
    url(r'^list/$', ProductImageListAPIView.as_view(), name='api-product-image-list'),
    url(r'^delete/(?P<pk>\d+)', ProductImageDeleteAPIView.as_view(), name='api-product-image-delete')
]
