# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView
)

from app.models import ProductImage

from .serializers import (
    ProductImageCreateSerializer,
    ProductImageSerializer,
    ProductImageDeleteSerializer
)


class ProductImageCreateAPIView(CreateAPIView):
    serializer_class = ProductImageCreateSerializer
    queryset = ProductImage.objects.all()

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class ProductImageListAPIView(ListAPIView):
    serializer_class = ProductImageSerializer
    queryset = ProductImage.objects.all()

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class ProductImageDeleteAPIView(DestroyAPIView):
    serializer_class = ProductImageDeleteSerializer
    queryset = ProductImage.objects.all()

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            image = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, image):
            return get_not_owner_response()

        product = image.product
        if os.path.isfile(image.image.path):
            os.remove(image.image.path)
        image.delete()

        remaining_images = ProductImage.objects.filter(product_id=product.id)
        if len(remaining_images) > 0:
            import pdb
            pdb.set_trace()
            product.cover_image = remaining_images.first()

            for index in range(len(remaining_images)):
                remaining_image = remaining_images[index]
                remaining_image.order = index + 1
                remaining_image.save()

        return get_deleted_successfully_response()
