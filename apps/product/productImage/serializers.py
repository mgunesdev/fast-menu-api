# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer

from app.models import ProductImage
from apps.helper.Base64ImageField import Base64ImageField


class ProductImageCreateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, allow_empty_file=True, use_url=True,
    )

    class Meta:
        model = ProductImage
        fields = ['id', 'product', 'image']


class ProductImageSerializer(ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ['id', 'product', 'image', 'order']


class ProductImageDeleteSerializer(ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ['id']
