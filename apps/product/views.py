# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from apps.helper.ApiViewHelper import checkDataGetSerializer
from apps.helper.StringHelper import getCapitalizedString

from app.models import Product, ProductImage, User, Menu, Category, Price, PreparingTime, Ingredient, \
    ProductIngredient
from .serializers import (
    ProductCreateSerializer,
    ProductListSerializer,
    ProductUpdateSerializer,
    ProductDetailSerializer,
    ProductDeleteSerializer,
    MyProductListSerializer)


class ProductCreateAPIView(CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductCreateSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        if not is_authenticated(request):
            return get_not_authenticated_response()

        if request.data.get("menu"):
            if not check_owner_for_model(request, Menu.objects.get(id=request.data["menu"])):
                return get_not_owner_response()
        try:
            serializer = checkDataGetSerializer(self, data)
            headers = self.get_success_headers(serializer.data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        price = data.get("price")
        if price is None:
            return get_bad_request_key_response("price")

        time = data.get("time")
        if time is None:
            return get_bad_request_key_response("time")

        ingredients = data.get("ingredients")
        if ingredients is None:
            return get_bad_request_key_response("ingredients")

        new_product = Product()
        title = data["title"]
        menu = Menu.objects.get(id=data["menu"])
        category = Category.objects.get(id=data["category"])

        new_product.title = title
        new_product.menu = menu
        new_product.category = category

        description = data["description"]
        new_product.description = description

        calorie = data.get("calorie")
        if calorie:
            new_product.calorie = calorie

        order = data.get("order")
        if order:
            new_product.order = order

        new_product.save()

        price = data.get("price")
        amount = price.get("amount")
        currency = price.get("currency")
        new_price = Price()
        new_price.amount = amount
        new_price.currency = currency
        new_price.product = new_product
        new_price.save()

        time = data.get("time")
        min_value = time.get("min_value")
        max_value = time.get("max_value")

        new_time = PreparingTime()
        new_time.min_value = min_value
        new_time.max_value = max_value
        new_time.product = new_product
        new_time.save()

        for ingredient in ingredients:
            new_product_ingredient = ProductIngredient()
            new_product_ingredient.product = new_product

            name = ingredient["name"]
            db_ingredient = Ingredient.objects.filter(name__iexact=getCapitalizedString(name)).first()
            if db_ingredient is not None:
                new_product_ingredient.ingredient = db_ingredient
            else:
                new_ingredient = Ingredient()
                new_ingredient.name = getCapitalizedString(name)
                new_ingredient.save()
                new_product_ingredient.ingredient = new_ingredient
            new_product_ingredient.save()

        return get_created_successfully_response(ProductCreateSerializer(new_product).data)


class ProductListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class MyProductListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = MyProductListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        current_set = self.queryset
        params = self.request.query_params
        branch_id = params.get('branch', None)
        menu_id = params.get('menu', None)
        category_id = params.get('category', None)
        status = params.get('status', None)

        if status is not None:
            current_set = current_set.filter(status=status)

        if branch_id is not None:
            current_set = current_set.filter(menu__branch_id=branch_id)

        if menu_id is not None:
            current_set = current_set.filter(menu_id=menu_id)

        if category_id is not None:
            current_set = current_set.filter(category_id=category_id)

        if self.request.user.parent is None:
            return current_set.filter(menu__branch__company__user_id=self.request.user.id)
        else:
            return current_set.filter(menu__branch__user_id=self.request.user.id)


class ProductDetailAPIView(RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductDetailSerializer

    def get(self, request, *args, **kwargs):
        product = Product.objects.filter(id=kwargs["pk"]).first()
        if product is None:
            return get_not_found_response()
        return get_detail_successfully_response(ProductDetailSerializer(product, context={'request': request}).data)


class ProductUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductUpdateSerializer

    def put(self, request, *args, **kwargs):
        data = request.data
        try:
            current_product = self.get_object()
        except Exception:
            return get_not_found_response()

        if not is_authenticated(request):
            return get_not_authenticated_response()

        if not check_owner_for_model(request, current_product):
            return get_not_owner_response()

        try:
            serializer = checkDataGetSerializer(self, data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        price = data.get("price")
        if price is None:
            return get_bad_request_key_response("price")

        time = data.get("time")
        if time is None:
            return get_bad_request_key_response("time")

        ingredients = data.get("ingredients")
        if ingredients is None:
            return get_bad_request_key_response("ingredients")

        title = data["title"]
        current_product.title = title

        description = data["description"]
        current_product.description = description

        calorie = data.get("calorie")
        if calorie:
            current_product.calorie = calorie

        order = data.get("order")
        if order:
            current_product.order = order

        current_price = Price.objects.filter(product_id=current_product.id).first()
        price = data.get("price")
        amount = price.get("amount")
        currency = price.get("currency")
        if current_price:
            current_price.amount = amount
            current_price.currency = currency
            current_price.save()
        else:

            new_price = Price()
            new_price.amount = amount
            new_price.currency = currency
            new_price.product = current_product
            new_price.save()

        current_time = PreparingTime.objects.filter(product_id=current_product.id).first()
        time = data.get("time")
        min_value = time.get("min_value")
        max_value = time.get("max_value")
        if current_time:
            current_time.min_value = min_value
            current_time.max_value = max_value
            current_time.save()
        else:
            new_time = PreparingTime()
            new_time.min_value = min_value
            new_time.max_value = max_value
            new_time.product = current_product
            new_time.save()

        current_product_ingredients = ProductIngredient.objects.filter(product_id=current_product.id)
        ingredients = data.get("ingredients")
        for ingredient in ingredients:

            ingredient_id = ingredient.get("id")
            name = ingredient.get("name")

            if ingredient_id:
                is_contains_current = False
                for current_product_ingredient in current_product_ingredients:
                    if ingredient_id == current_product_ingredient.ingredient.id:
                        is_contains_current = True

                if not is_contains_current:
                    new_product_ingredient = ProductIngredient()
                    new_product_ingredient.product = current_product

                    db_ingredient = Ingredient.objects.filter(name__iexact=getCapitalizedString(name)).first()
                    if db_ingredient is not None:
                        new_product_ingredient.ingredient = db_ingredient
                    else:
                        new_ingredient = Ingredient()
                        new_ingredient.name = getCapitalizedString(name)
                        new_ingredient.save()
                        new_product_ingredient.ingredient = new_ingredient
                    new_product_ingredient.save()

            else:
                new_product_ingredient = ProductIngredient()
                new_product_ingredient.product = current_product

                db_ingredient = Ingredient.objects.filter(name__iexact=getCapitalizedString(name)).first()
                if db_ingredient is not None:
                    new_product_ingredient.ingredient = db_ingredient
                else:
                    new_ingredient = Ingredient()
                    new_ingredient.name = getCapitalizedString(name)
                    new_ingredient.save()
                    new_product_ingredient.ingredient = new_ingredient

                new_product_ingredient.save()

        for current_product_ingredient in current_product_ingredients:
            isContainsNews = False
            for ingredient in ingredients:
                ingredient_id = ingredient.get("id")
                if current_product_ingredient.ingredient.id == ingredient_id:
                    isContainsNews = True

            if not isContainsNews:
                current_product_ingredient.delete()

        current_product.save()
        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

    def patch(self, request, *args, **kwargs):
        data = request.data

        try:
            product = self.get_object()
        except Exception:
            return get_not_found_response()

        if not is_authenticated(request):
            return get_not_authenticated_response()

        if not check_owner_for_model(request, product):
            return get_not_owner_response()

        try:
            mode = request.data["mode"]
        except Exception:
            return get_bad_request_key_response("mode")

        if mode == "change-orders":
            new_orders = request.data["new-orders"]
            for id in new_orders:
                int_id = int(id)
                order = new_orders[id]
                product_image = ProductImage.objects.filter(id=int_id).first()
                product_image.order = order
                product_image.save()
                if order == 1:
                    product.cover_image = product_image.image
                    product.save()
        elif mode == "delete-image":
            image_id = request.data["image-id"]
            image_list = list(product.product_image.all())
            del_image = ProductImage.objects.filter(id=image_id)
            del_image = del_image.first()
            index = image_list.index(del_image)
            image_list.remove(del_image)

            for i in range(index, len(image_list)):
                product_image = image_list[i]
                if product_image.order == 1:
                    break
                product_image.order = product_image.order - 1
                product_image.save()

            del_image.delete()

            if len(image_list) > 0:
                product.cover_image = image_list[0].id
            else:
                product.cover_image = None
            product.save()
        elif mode == "update-status":
            status = data.get("status")
            if status is not None:
                product.status = status
                product.save()
            else:
                return get_bad_request_key_response("status")
        elif mode == "update-trend-status":
            is_trend = data.get("is_trend")
            if is_trend is not None:
                if is_trend is True and Product.objects.filter(menu_id=product.menu.id, is_trend=True).count() > 4:
                    return get_bad_request_message_response("Bir menü için en fazla 5 trend ürün tanımlanabilir!")

                product.is_trend = is_trend
                product.save()
            else:
                return get_bad_request_key_response("update-trend-status")
        elif mode == "update-taste-status":
            is_should_taste = data.get("is_should_taste")
            if is_should_taste is not None:
                if is_should_taste is True and Product.objects.filter(menu_id=product.menu.id,
                                                                      is_should_taste=True).count() > 4:
                    return get_bad_request_message_response("Bir menü için en fazla 5 önerilen ürün tanımlanabilir!")

                product.is_should_taste = is_should_taste
                product.save()
            else:
                return get_bad_request_key_response("update-taste-status")

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class ProductDeleteAPIView(DestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductDeleteSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            product = self.get_object()
        except Exception:
            return get_not_found_response()

        if not is_authenticated(request):
            return get_not_authenticated_response()

        if not check_owner_for_model(request, product):
            return get_not_owner_response()

        for branch_image in ProductImage.objects.filter(product_id=product.id):
            if os.path.isfile(branch_image.image.path):
                os.remove(branch_image.image.path)

        product.delete()

        return get_deleted_successfully_response()
