# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.productIngredients.views import (
    ProductIngredientCreateAPIView,
    ProductIngredientListAPIView,
    ProductIngredientDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', ProductIngredientCreateAPIView.as_view(), name='api-product-ingredient-create'),
    url(r'^delete/(?P<pk>\d+)', ProductIngredientDeleteAPIView.as_view(), name='api-product-ingredient-delete'),
    url(r'^list/$', ProductIngredientListAPIView.as_view(), name='api-product-ingredient-list'),
]
