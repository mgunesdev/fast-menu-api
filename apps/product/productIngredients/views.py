# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
)

from app.models import ProductIngredient
from .serializers import (
    ProductIngredientSerializer,
    ProductIngredientDeleteSerializer
)


class ProductIngredientCreateAPIView(CreateAPIView):
    queryset = ProductIngredient.objects.all()
    serializer_class = ProductIngredientSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class ProductIngredientListAPIView(ListAPIView):
    queryset = ProductIngredient.objects.all()
    serializer_class = ProductIngredientSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class ProductIngredientDeleteAPIView(DestroyAPIView):
    queryset = ProductIngredient.objects.all()
    serializer_class = ProductIngredientDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            product_ingredient = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, product_ingredient):
            return get_not_owner_response()

        product_ingredient.delete()

        return get_deleted_successfully_response()
