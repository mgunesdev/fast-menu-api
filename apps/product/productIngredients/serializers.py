# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.ingredients.serializers import IngredientSerializer
from app.models import ProductIngredient, Ingredient


class ProductIngredientSerializer(ModelSerializer):
    class Meta:
        model = ProductIngredient
        fields = ['id',
                  'ingredient',
                  'product'
                  ]


class ProductIngredientDetailSerializer(ModelSerializer):
    ingredient = SerializerMethodField()

    class Meta:
        model = ProductIngredient
        fields = ['id',
                  'ingredient',
                  'product'
                  ]

    def get_ingredient(self, obj):
        c_qs = Ingredient.objects.get(id=obj.ingredient.id)
        ingredient = IngredientSerializer(c_qs).data
        return ingredient


class ProductIngredientDeleteSerializer(ModelSerializer):
    class Meta:
        model = ProductIngredient
        fields = ['id']
