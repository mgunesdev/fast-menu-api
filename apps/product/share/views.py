# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView
)

from app.models import ProductShare
from apps.helper.responseProcess.ResponseHelper import *
from .serializers import (
    ProductShareSerializer
)


class ProductShareCreateAPIView(CreateAPIView):
    queryset = ProductShare.objects.all()
    serializer_class = ProductShareSerializer

    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
