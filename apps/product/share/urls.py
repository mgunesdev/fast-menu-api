# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.share.views import (
    ProductShareCreateAPIView
)

urlpatterns = [
    url(r'^create/$', ProductShareCreateAPIView.as_view(), name='api-product-share-create')
]
