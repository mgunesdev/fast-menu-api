# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer

from app.models import ProductShare


class ProductShareSerializer(ModelSerializer):
    class Meta:
        model = ProductShare
        fields = [
            'id',
            'user',
            'product',
            'share_date'
        ]
