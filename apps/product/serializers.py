# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from builtins import round, type, len

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from django.db.models import Avg
from django.contrib.auth.models import AnonymousUser

from apps.product.preparingTime.serializers import PreparingTimeSerializer
from apps.product.price.serializers import PriceSerializer
from apps.product.comment.serializers import ProductCommentDetailSerializer
from apps.product.productImage.serializers import ProductImageSerializer
from app.models import Product, Price, PreparingTime, ProductIngredient, ProductImage, ProductComment, ProductFav, \
    ProductShare, Company


class ProductCreateSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'menu',
            'description',
            'category',
            'title',
            'calorie',
            'order',
            'status'
        ]


class ProductListSerializer(ModelSerializer):
    price = SerializerMethodField()
    average_rate = SerializerMethodField()
    company = SerializerMethodField()
    images = SerializerMethodField()
    fav = SerializerMethodField()

    class Meta:
        model = Product
        fields = ['id',
                  'menu',
                  'title',
                  'order',
                  'price',
                  'average_rate',
                  'images',
                  'fav',
                  'company',
                  'status'
                  ]

    def get_price(self, obj):
        price = Price.objects.filter(product_id=obj.id).first()
        if price is None:
            return None
        return PriceSerializer(price).data

    def get_average_rate(self, obj):
        average_rate = ProductComment.objects.filter(product_id=obj.id, parent=None).aggregate(Avg('rate')).get(
            "rate__avg")
        if average_rate is not None:
            return round(
                average_rate, 1)
        return None

    def get_company(self, obj):
        return {"name": Company.objects.get(id=obj.menu.branch.company.id).name}

    def get_images(self, obj):
        c_qs = ProductImage.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        images = ProductImageSerializer(c_qs, many=True).data
        return images

    def get_fav(self, obj):
        if type(self.context['request'].user) is not AnonymousUser:
            try:
                fav = ProductFav.objects.get(product_id=obj.id, user_id=self.context['request'].user.id)
                return {"id": fav.id}
            except Exception:
                pass

        return None


class MyProductListSerializer(ModelSerializer):
    price = SerializerMethodField()
    images = SerializerMethodField()
    time = SerializerMethodField()
    ingredients = SerializerMethodField()

    class Meta:
        model = Product
        fields = ['id',
                  'menu',
                  'title',
                  'order',
                  'price',
                  'average_rate',
                  'images',
                  'status',
                  'calorie',
                  'order',
                  'time',
                  'description',
                  'ingredients',
                  'is_trend',
                  'is_should_taste'
                  ]

    def get_price(self, obj):
        price = Price.objects.filter(product_id=obj.id).first()
        if price is None:
            return None
        return PriceSerializer(price).data

    def get_images(self, obj):
        c_qs = ProductImage.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        images = ProductImageSerializer(c_qs, many=True).data
        return images

    def get_time(self, obj):
        time = PreparingTime.objects.filter(product_id=obj.id).first()
        if time is None:
            return None
        return PreparingTimeSerializer(time).data

    def get_ingredients(self, obj):
        c_qs = ProductIngredient.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        ingredients = []
        for product_ingredient in c_qs:
            new_json = {"id": product_ingredient.ingredient.id,
                        "name": product_ingredient.ingredient.name}

            ingredients.append(new_json)
        return ingredients


class ProductDetailSerializer(ModelSerializer):
    price = SerializerMethodField()
    preparing_time = SerializerMethodField()
    ingredients = SerializerMethodField()
    images = SerializerMethodField()
    comments = SerializerMethodField()
    comment_count = SerializerMethodField()
    average_rate = SerializerMethodField()
    like_count = SerializerMethodField()
    share_count = SerializerMethodField()
    fav = SerializerMethodField()

    class Meta:
        model = Product
        fields = ['id',
                  'menu',
                  'category',
                  'title',
                  'description',
                  'calorie',
                  'cover_image',
                  'price',
                  'images',
                  'preparing_time',
                  'ingredients',
                  "comments",
                  "comment_count",
                  'average_rate',
                  'like_count',
                  'share_count',
                  'fav',
                  'status'
                  ]

    def get_price(self, obj):
        price = Price.objects.filter(product_id=obj.id).first()
        if price is None:
            return None
        return PriceSerializer(price).data

    def get_preparing_time(self, obj):
        time = PreparingTime.objects.filter(product_id=obj.id).first()
        if time is None:
            return None
        return PreparingTimeSerializer(time).data

    def get_ingredients(self, obj):
        c_qs = ProductIngredient.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        ingredients = []
        for product_ingredient in c_qs:
            new_json = {"id": product_ingredient.ingredient.id,
                        "name": product_ingredient.ingredient.name}

            ingredients.append(new_json)
        return ingredients

    def get_images(self, obj):
        c_qs = ProductImage.objects.filter(product_id=obj.id)
        if c_qs.count() == 0:
            return None
        images = ProductImageSerializer(c_qs, many=True).data
        return images

    def get_comments(self, obj):
        c_qs = ProductComment.objects.filter(product_id=obj.id, parent=None)[:2]
        if c_qs.count() == 0:
            return None
        comments = ProductCommentDetailSerializer(c_qs, many=True).data
        return comments

    def get_comment_count(self, obj):
        return ProductComment.objects.filter(product_id=obj.id).count()

    def get_average_rate(self, obj):
        c_qs = ProductComment.objects.filter(product_id=obj.id, parent=None)
        count = len(c_qs)
        if count == 0:
            return 0
        rate = 0
        for comment in c_qs:
            rate += comment.rate
        return round(rate / count, 1)

    def get_like_count(self, obj):
        return ProductFav.objects.filter(product_id=obj.id).count()

    def get_share_count(self, obj):
        return ProductShare.objects.filter(product_id=obj.id).count()

    def get_fav(self, obj):
        if type(self.context['request'].user) is not AnonymousUser:
            try:
                fav = ProductFav.objects.get(product_id=obj.id, user_id=self.context['request'].user.id)
                return {"id": fav.id}
            except Exception:
                pass

        return None


class ProductUpdateSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id',
                  'title',
                  'description',
                  'calorie',
                  'order',
                  'status',
                  'is_trend',
                  'is_should_taste']


class ProductDeleteSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id']
