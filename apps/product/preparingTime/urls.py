# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.preparingTime.views import (
    PreparingTimeCreateAPIView,
    PreparingTimeListAPIView,
    PreparingTimeDetailAPIView,
    PreparingTimeDeleteAPIView,
    PreparingTimeUpdateAPIView
)

urlpatterns = [
    url(r'^create/$', PreparingTimeCreateAPIView.as_view(), name='api-preparing-time-create'),
    url(r'^detail/(?P<pk>\d+)', PreparingTimeDetailAPIView.as_view(), name='api-preparing-time-detail'),
    url(r'^delete/(?P<pk>\d+)', PreparingTimeDeleteAPIView.as_view(), name='api-preparing-time-delete'),
    url(r'^list/$', PreparingTimeListAPIView.as_view(), name='api-preparing-time-list'),
    url(r'^edit/(?P<pk>\d+)', PreparingTimeUpdateAPIView.as_view(), name='api-preparing-time-edit')
]
