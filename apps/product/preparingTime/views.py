# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import unicode_literals
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView,
    RetrieveAPIView
)

from app.models import PreparingTime

from .serializers import (
    PreparingTimeSerializer,
    PreparingTimeDeleteSerializer
)


class PreparingTimeCreateAPIView(CreateAPIView):
    queryset = PreparingTime.objects.all()
    serializer_class = PreparingTimeSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PreparingTimeListAPIView(ListAPIView):
    queryset = PreparingTime.objects.all()
    serializer_class = PreparingTimeSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class PreparingTimeUpdateAPIView(RetrieveUpdateAPIView):
    queryset = PreparingTime.objects.all()
    serializer_class = PreparingTimeSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            time = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, time):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PreparingTimeDetailAPIView(RetrieveAPIView):
    queryset = PreparingTime.objects.all()
    serializer_class = PreparingTimeSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        time = PreparingTime.objects.filter(id=kwargs["pk"]).first()
        if time is None:
            return get_not_found_response()
        return get_detail_successfully_response(PreparingTimeSerializer(time).data)


class PreparingTimeDeleteAPIView(DestroyAPIView):
    queryset = PreparingTime.objects.all()
    serializer_class = PreparingTimeDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            time = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, time):
            return get_not_owner_response()

        time.delete()

        return get_deleted_successfully_response()
