# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import PreparingTime


class PreparingTimeSerializer(ModelSerializer):
    class Meta:
        model = PreparingTime
        fields = [
            'id',
            'product',
            'min_value',
            'max_value'
        ]


class PreparingTimeDeleteSerializer(ModelSerializer):
    class Meta:
        model = PreparingTime
        fields = [
            'id'
        ]
