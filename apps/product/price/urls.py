# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.product.price.views import (
    PriceCreateAPIView,
    PriceListAPIView,
    PriceDetailAPIView,
    PriceDeleteAPIView,
    PriceUpdateAPIView
)

urlpatterns = [
    url(r'^create/$', PriceCreateAPIView.as_view(), name='api-price-create'),
    url(r'^detail/(?P<pk>\d+)', PriceDetailAPIView.as_view(), name='api-price-detail'),
    url(r'^delete/(?P<pk>\d+)', PriceDeleteAPIView.as_view(), name='api-price-delete'),
    url(r'^list/$', PriceListAPIView.as_view(), name='api-price-list'),
    url(r'^edit/(?P<pk>\d+)', PriceUpdateAPIView.as_view(), name='api-price-edit')
]
