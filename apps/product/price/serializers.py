# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Price


class PriceSerializer(ModelSerializer):
    class Meta:
        model = Price
        fields = ['id',
                  'product',
                  'amount',
                  'currency'
                  ]


class PriceDeleteSerializer(ModelSerializer):
    class Meta:
        model = Price
        fields = ['id']
