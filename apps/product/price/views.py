# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView,
    RetrieveAPIView
)

from app.models import Price

from .serializers import (
    PriceSerializer,
    PriceDeleteSerializer
)


class PriceCreateAPIView(CreateAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PriceListAPIView(ListAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class PriceUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            price = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, price):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PriceDetailAPIView(RetrieveAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        price = Price.objects.filter(id=kwargs["pk"]).first()
        if price is None:
            return get_not_found_response()
        return get_detail_successfully_response(PriceSerializer(price).data)


class PriceDeleteAPIView(DestroyAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            price = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, price):
            return get_not_owner_response()

        price.delete()

        return get_deleted_successfully_response()
