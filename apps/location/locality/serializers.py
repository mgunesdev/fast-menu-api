# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Locality
from apps.helper.serializers.dynamic_fields import DynamicFieldsSerializerMixin


class LocalitySerializer(DynamicFieldsSerializerMixin, ModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()

    class Meta:
        model = Locality
        fields = '__all__'

    def get_latitude(self, obj):
        if obj.latitude:
            return float(obj.latitude)
        return None

    def get_longitude(self, obj):
        if obj.longitude:
            return float(obj.longitude)
        return None


class LocalityShortSerializer(ModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()

    class Meta:
        model = Locality
        fields = [
            'latitude',
            'longitude',
            'code',
            'parentcode',
            'name'
        ]

    def get_latitude(self, obj):
        return float(obj.latitude)

    def get_longitude(self, obj):
        return float(obj.longitude)
