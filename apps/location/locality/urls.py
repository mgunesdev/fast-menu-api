# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.location.locality.views import (
    LocalityListAPIView,
    LocalityDetailAPIView,
    LocalityInsertAPIView
)

urlpatterns = [
    url(r'^list/$', LocalityListAPIView.as_view(), name='api-locality-list'),
    url(r'^detail/(?P<pk>\d+)', LocalityDetailAPIView.as_view(), name='api-locality-detail'),
    url(r'^insert/$', LocalityInsertAPIView.as_view(), name='api-locality-insert')

]
