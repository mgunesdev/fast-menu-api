# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError
import requests
import json
import time

from rest_framework.generics import (
    RetrieveAPIView,
    ListAPIView,
)

from app.decorators import check_auth
from app.models import Locality, District
from apps.helper.responseProcess.ResponseHelper import *
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin
from apps.helper.CustomPagination import LocationPagination
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import (
    LocalitySerializer
)


class LocalityListAPIView(DynamicFieldsViewMixin, ListAPIView):
    queryset = Locality.objects.all()
    serializer_class = LocalitySerializer
    pagination_class = LocationPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['parentcode']

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class LocalityDetailAPIView(RetrieveAPIView):
    queryset = Locality.objects.all()
    serializer_class = LocalitySerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Locality.objects.filter(code=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class LocalityInsertAPIView(ListAPIView):
    queryset = Locality.objects.all()
    serializer_class = LocalitySerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        locality_list = Locality.objects.filter(polygon=None, is_should_update=False)[:500]
        index = 0
        for locality in locality_list:
            index += 1
            url = "https://nominatim.openstreetmap.org/search.php?q={}&polygon_geojson=1&viewbox=&format=json".format(
                locality.slug)
            print(url)
            time.sleep(1)
            response = requests.get(url)
            my_json = json.loads(response.content.decode('utf8'))
            print (index)
            is_should_update = True
            if len(my_json) > 0:
                for item in my_json:
                    osm_type = item.get("osm_type")
                    name = item.get("display_name").replace(" ", "")
                    name_array = name.split(",")
                    district = name_array[1]
                    if osm_type == "relation" and district == District.objects.get(code=locality.parentcode).name:
                        city_json = item
                        geoJson = city_json.get("geojson")
                        if geoJson.get("type") == "Polygon":
                            locality.polygon = geoJson.get("coordinates")[0]
                            locality.save()
                            is_should_update = False
                            print(url, len(geoJson.get("coordinates")[0]))
                        elif geoJson.get("type") == "MultiPolygon":
                            locality.polygon = geoJson.get("coordinates")[0][0]
                            locality.save()
                            is_should_update = False
                            print(url, len(geoJson.get("coordinates")[0][0]))
                        break

            if is_should_update:
                locality.is_should_update = is_should_update
                locality.save()

        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)
