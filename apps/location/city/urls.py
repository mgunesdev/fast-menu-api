# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.location.city.views import (
    CityListAPIView,
    CityDetailAPIView,
    CityInsertAPIView
)

urlpatterns = [
    url(r'^list/$', CityListAPIView.as_view(), name='api-city-list'),
    url(r'^detail/(?P<pk>\d+)', CityDetailAPIView.as_view(), name='api-city-detail'),
    url(r'^insert/$', CityInsertAPIView.as_view(), name='api-city-insert')

]
