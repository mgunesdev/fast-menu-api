# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError
import requests
import json

from app.decorators import check_auth
from apps.helper.polygonChecker import point_in_poly

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.models import City, District
from apps.helper.responseProcess.ResponseHelper import *
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin
from apps.helper.CustomPagination import LocationPagination

from .serializers import (
    CitySerializer
)

from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin


class CityListAPIView(DynamicFieldsViewMixin, ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = LocationPagination

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class CityDetailAPIView(RetrieveAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if City.objects.filter(code=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class CityInsertAPIView(ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        # city_list = City.objects.filter(polygon=None)
        # index = 0
        # for city in city_list:
        #     index += 1
        #     url = "https://nominatim.openstreetmap.org/search.php?q={}&polygon_geojson=1&viewbox=&format=json".format(
        #         city.name)
        #     print(url)
        #     response = requests.get(url)
        #     my_json = json.loads(response.content.decode('utf8'))
        #     if len(my_json) > 0:
        #         for item in my_json:
        #             osm_type = item.get("osm_type")
        #             name = item.get("display_name").replace(" ", "")
        #             name_array = name.split(",")
        #             country = name_array[len(name_array) - 1]
        #             if osm_type == "relation" and country == "Türkiye":
        #                 city_json = item
        #                 geoJson = city_json.get("geojson")
        #                 if geoJson.get("type") == "Polygon":
        #                     city.polygon = geoJson.get("coordinates")[0]
        #                     city.save()
        #                     print(url, len(geoJson.get("coordinates")[0]))
        #                 elif geoJson.get("type") == "MultiPolygon":
        #                     city.polygon = geoJson.get("coordinates")[0][0]
        #                     city.save()
        #                 break

        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)
