# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.decorators import check_auth
from apps.helper.polygonChecker import get_city_district_from_latlng
from rest_framework.exceptions import ValidationError
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D

from rest_framework.generics import (
    RetrieveAPIView,
    ListAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
    GenericAPIView,
    DestroyAPIView
)

from app.models import Location, City, District, Locality
from apps.helper.responseProcess.ResponseHelper import *
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin
from apps.helper.CustomPagination import LocationPagination
from django_filters.rest_framework import DjangoFilterBackend

from apps.location.locality.serializers import LocalityShortSerializer
from .serializers import (
    LocationSerializer,
    LocationCreateSerializer,
    LocationSearchSerializer
)


class LocationListAPIView(DynamicFieldsViewMixin, ListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    pagination_class = LocationPagination

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        return self.queryset.filter(user__id=self.request.user.id)


class LocationSearchAPIView(ListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSearchSerializer
    pagination_class = LocationPagination

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        point = self.request.query_params.get('point', None)
        keyword = self.request.query_params.get('keyword', None)
        criteria = {}
        if point is not None and "," in point:
            point_array = point.split(",")
            lat = float(point_array[0])
            lng = float(point_array[1])
            point = Point(lat, lng)
            radius = 70
            criteria["point__distance_lte"] = (point, D(km=radius))
        if keyword:
            criteria["name__istartswith"] = keyword

        criteria["longitude__isnull"] = False
        criteria["latitude__isnull"] = False

        city_list = City.objects.filter(**criteria)
        district_list = District.objects.filter(**criteria)
        locality_list = Locality.objects.filter(**criteria)

        from itertools import chain
        result_list = list(chain(locality_list, district_list, city_list))
        return result_list


class LocationCreateAPIView(CreateAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            result = self.create(request, *args, **kwargs).data
            city = City.objects.get(code=result.get("city"))
            result["city"] = {"code": city.code,
                              "name": city.name,
                              "latitude": city.latitude,
                              "longitude": city.longitude}

            district = District.objects.get(code=result.get("district"))
            result["district"] = {"code": district.code,
                                  "name": district.name,
                                  "latitude": district.latitude,
                                  "longitude": district.longitude}

            locality = Locality.objects.get(code=result.get("locality"))
            result["locality"] = {"code": locality.code,
                                  "name": locality.name,
                                  "latitude": locality.latitude,
                                  "longitude": locality.longitude}

            return get_created_successfully_response(result)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class LocationDetailAPIView(RetrieveAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Location.objects.filter(id=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class LocationFindAPIView(RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        return self.get_queryset()

    def get_queryset(self):
        point = self.request.query_params.get('point', None)
        if point is not None and "," in point:
            point_array = point.split(",")
            lat = float(point_array[0])
            lng = float(point_array[1])
            city, district, locality = get_city_district_from_latlng(lng, lat)
            response = {"latitude": lat, "longitude": lng}
            if city is not None:
                response["city"] = {"code": city.code, "name": city.name, "latitude": city.latitude,
                                    "longitude": city.longitude}
                if district is not None:
                    response["district"] = {"code": district.code, "name": district.name, "latitude": district.latitude,
                                            "longitude": district.longitude}

                    if locality is not None:
                        response["locality"] = {"code": locality.code, "name": locality.name,
                                                "latitude": locality.latitude,
                                                "longitude": locality.longitude}

            return get_detail_successfully_response(response)

        return get_bad_request_key_response("point")


class LocationUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationCreateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        try:
            result = self.update(request, *args, **kwargs).data
            city = City.objects.get(code=result.get("city"))
            result["city"] = {"code": city.code,
                              "name": city.name,
                              "latitude": city.latitude,
                              "longitude": city.longitude}

            district = District.objects.get(code=result.get("district"))
            result["district"] = {"code": district.code,
                                  "name": district.name,
                                  "latitude": district.latitude,
                                  "longitude": district.longitude}

            locality = Locality.objects.get(code=result.get("locality"))
            result["locality"] = {"code": locality.code,
                                  "name": locality.name,
                                  "latitude": locality.latitude,
                                  "longitude": locality.longitude}

            return get_updated_successfully_response(result)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class LocationDeleteAPIView(DestroyAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            location = self.get_object()
        except Exception:
            return get_not_found_response()

        location.delete()

        return get_deleted_successfully_response()
