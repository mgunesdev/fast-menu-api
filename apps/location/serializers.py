# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from builtins import type

from django.contrib.auth.models import AnonymousUser
from rest_framework.fields import SerializerMethodField
from rest_framework import serializers

from app.models import Location, District, City
from apps.helper.serializers.dynamic_fields import DynamicFieldsSerializerMixin


class LocationCreateSerializer(serializers.ModelSerializer):
    user = SerializerMethodField()

    class Meta:
        model = Location
        fields = '__all__'

    def get_user(self, obj):
        print (type(obj))
        if type(self.context['request'].user) is not AnonymousUser:
            obj.user = self.context['request'].user
            obj.save()
            return self.context['request'].user.id

        return None


class LocationSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()
    locality = SerializerMethodField()
    district = SerializerMethodField()
    city = SerializerMethodField()

    class Meta:
        model = Location
        fields = ['id',
                  'user',
                  'address',
                  "locality",
                  "district",
                  "city",
                  "latitude",
                  "longitude",
                  "point",
                  "created_at",
                  "updated_at"]

    def get_latitude(self, obj):
        if obj.latitude:
            return float(obj.latitude)
        return None

    def get_longitude(self, obj):
        if obj.longitude:
            return float(obj.longitude)
        return None

    def get_locality(self, obj):
        if obj.locality:
            locality = {"code": obj.locality.code,
                        "name": obj.locality.name,
                        "latitude": obj.locality.latitude,
                        "longitude": obj.locality.longitude}
            return locality
        return None

    def get_district(self, obj):
        if obj.locality:
            try:
                district = District.objects.get(code=obj.locality.parentcode)
                return {"code": district.code,
                        "name": district.name,
                        "latitude": district.latitude,
                        "longitude": district.longitude}
            except Exception:
                return None
        return None

    def get_city(self, obj):
        if obj.locality:
            try:
                district = District.objects.get(code=obj.locality.parentcode)
                city = City.objects.get(code=district.parentcode)
                return {"code": city.code,
                        "name": city.name,
                        "latitude": city.latitude,
                        "longitude": city.longitude}
            except Exception:
                return None

        return None


class LocationSearchSerializer(serializers.Serializer):
    code = serializers.IntegerField()
    name = serializers.CharField()
    latitude = serializers.CharField()
    longitude = serializers.CharField()
