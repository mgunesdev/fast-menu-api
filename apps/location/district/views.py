# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError
import requests
import json
import time

from rest_framework.generics import (
    RetrieveAPIView,
    ListAPIView,
)

from app.decorators import check_auth
from app.models import District
from apps.helper.responseProcess.ResponseHelper import *

from .serializers import (
    DistrictSerializer
)

from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin
from django_filters.rest_framework import DjangoFilterBackend
from apps.helper.CustomPagination import LocationPagination


class DistrictListAPIView(DynamicFieldsViewMixin, ListAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['parentcode']
    pagination_class = LocationPagination

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class DistrictDetailAPIView(RetrieveAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if District.objects.filter(code=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class DistrictInsertAPIView(ListAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        # district_list = District.objects.filter(polygon=None)
        # index = 0
        # for district in district_list:
        #     index += 1
        #     url = "https://nominatim.openstreetmap.org/search.php?q={}&polygon_geojson=1&viewbox=&format=json".format(
        #         district.slug)
        #     print(url)
        #     time.sleep(0.1)
        #     response = requests.get(url)
        #     my_json = json.loads(response.content.decode('utf8'))
        #     if len(my_json) > 0:
        #         for item in my_json:
        #             osm_type = item.get("osm_type")
        #             name = item.get("display_name").replace(" ", "")
        #             name_array = name.split(",")
        #             country = name_array[len(name_array) - 1]
        #             if osm_type == "relation" and country == "Türkiye":
        #                 city_json = item
        #                 geoJson = city_json.get("geojson")
        #                 if geoJson.get("type") == "Polygon":
        #                     district.polygon = geoJson.get("coordinates")[0]
        #                     district.save()
        #                     print(url, len(geoJson.get("coordinates")[0]))
        #                 elif geoJson.get("type") == "MultiPolygon":
        #                     district.polygon = geoJson.get("coordinates")[0][0]
        #                     district.save()
        #                     print(url, len(geoJson.get("coordinates")[0][0]))
        #
        #                 break

        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)
