# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import District

from apps.helper.serializers.dynamic_fields import DynamicFieldsSerializerMixin


class DistrictSerializer(DynamicFieldsSerializerMixin, ModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()

    class Meta:
        model = District
        fields = '__all__'

    def get_latitude(self, obj):
        return float(obj.latitude)

    def get_longitude(self, obj):
        return float(obj.longitude)


class DistrictShortSerializer(ModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()

    class Meta:
        model = District
        fields = [
            'latitude',
            'longitude',
            'code',
            'parentcode',
            'name'
        ]

    def get_latitude(self, obj):
        return float(obj.latitude)

    def get_longitude(self, obj):
        return float(obj.longitude)
