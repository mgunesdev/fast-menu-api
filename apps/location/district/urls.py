# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.location.district.views import (
    DistrictListAPIView,
    DistrictDetailAPIView,
    DistrictInsertAPIView
)

urlpatterns = [
    url(r'^list/$', DistrictListAPIView.as_view(), name='api-district-list'),
    url(r'^detail/(?P<pk>\d+)', DistrictDetailAPIView.as_view(), name='api-district-detail'),
    url(r'^insert/$', DistrictInsertAPIView.as_view(), name='api-district-insert')

]
