# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.location.views import (
    LocationListAPIView,
    LocationCreateAPIView,
    LocationDetailAPIView,
    LocationUpdateAPIView,
    LocationDeleteAPIView,
    LocationFindAPIView,
    LocationSearchAPIView
)

urlpatterns = [
    url(r'^list/$', LocationListAPIView.as_view(), name='api-location-list'),
    url(r'^search/$', LocationSearchAPIView.as_view(), name='api-location-search'),
    url(r'^create/$', LocationCreateAPIView.as_view(), name='api-location-create'),
    url(r'^detail/(?P<pk>\d+)', LocationDetailAPIView.as_view(), name='api-location-detail'),
    url(r'^find/', LocationFindAPIView.as_view(), name='api-location-detail'),
    url(r'^delete/(?P<pk>\d+)', LocationDeleteAPIView.as_view(), name='api-location-delete'),
    url(r'^edit/(?P<pk>\d+)', LocationUpdateAPIView.as_view(), name='api-location-edit')

]
