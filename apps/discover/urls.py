# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.discover.views import (
    NearByListAPIView,
    HiddenPlaceListAPIView,
    LastVisitedListAPIView,
    TrendListAPIView,
    TopListAPIView
)

urlpatterns = [
    url(r'^nearby/', NearByListAPIView.as_view(), name='api-discover-nearby'),
    url(r'^hidden-place/', HiddenPlaceListAPIView.as_view(), name='api-discover-hidden-place'),
    url(r'^last-visited/$', LastVisitedListAPIView.as_view(), name='api-last-visited-place'),
    url(r'^trend/', TrendListAPIView.as_view(), name='api-trend-product'),
    url(r'^top-list/', TopListAPIView.as_view(), name='api-top-list-product'),
]
