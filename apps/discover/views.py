# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from builtins import float

from datetime import date, timedelta

from django.contrib.gis.geos import Point, Polygon
from django.contrib.gis.measure import D
from django.db.models import F, Count, Avg, Q
from ipython_genutils.py3compat import annotate

from apps.branch.serializers import BranchListSerializer
from apps.helper.CustomPagination import DiscoverListPagination
from apps.helper.polygonChecker import get_city_district_from_latlng
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    ListAPIView
)

from app.models import Branch, QrCodeRead, Product
from apps.product.serializers import ProductListSerializer
from cacheops import cache, CacheMiss


class NearByListAPIView(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer
    pagination_class = DiscoverListPagination

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    # def get_queryset(self):
    #     point = self.request.query_params.get('point', None)
    #     if point is not None and "," in point:
    #         point_array = point.split(",")
    #         lat = float(point_array[0])
    #         lng = float(point_array[1])
    #         point = Point(lat, lng)
    #         radius = 2
    #         query_result = self.queryset.annotate(avg_rating=Avg('branch_comment_branch__rate')).filter(
    #             branch_comment_branch__rate__gte=3.5,
    #             location__point__distance_lte=(point, D(km=radius)))
    #         return query_result
    #
    #     return get_bad_request_key_response("point")


class HiddenPlaceListAPIView(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer
    pagination_class = DiscoverListPagination

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    # def get_queryset(self):
    #     point = self.request.query_params.get('point', None)
    #     if point is not None and "," in point:
    #         point_array = point.split(",")
    #         lat = float(point_array[0])
    #         lng = float(point_array[1])
    #         point = Point(lat, lng)
    #         radius = 2
    #         query_result = self.queryset.annotate(avg_rating=Avg('branch_comment_branch__rate')).annotate(
    #             comment_count=Count('branch_comment_branch')).filter(
    #             comment_count__lt=3,
    #             avg_rating__gte=3.0,
    #             location__point__distance_lte=(point, D(km=radius)))
    #
    #         return query_result
    #
    #     return get_bad_request_key_response("point")


class LastVisitedListAPIView(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer
    pagination_class = DiscoverListPagination

    def get(self, request, *args, **kwargs):
        if not is_authenticated(request):
            return get_not_authenticated_response()

        # branches = Branch.objects.filter(menu_branch__code_read_menu__user=self.request.user).order_by(
        #     "-menu_branch__code_read_menu__created_at")
        #
        # list_branch = []
        # for branch in branches:
        #     if branch not in list_branch:
        #         list_branch.append(branch)

        # return get_list_successfully_response(request.GET, BranchListSerializer(list_branch[:20], many=True).data)
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class TrendListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    pagination_class = DiscoverListPagination

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    # def get_queryset(self):
    #     point = self.request.query_params.get('point', None)
    #     if point is not None and "," in point:
    #         point_array = point.split(",")
    #         lat = float(point_array[0])
    #         lng = float(point_array[1])
    #         point = Point(lat, lng)
    #         radius = 10
    #         date_from = date.today() - timedelta(days=6)
    #         query_result = self.queryset.annotate(avg_rating=Avg('product_comment__rate')).annotate(
    #             comment_count=Count('product_comment',
    #                                 filter=Q(product_comment__comment_create_date__gte=date_from))).filter(
    #             comment_count__gt=20,
    #             avg_rating__gte=4.0,
    #             menu__branch__location__point__distance_lte=(point, D(km=radius)))
    #
    #         return query_result
    #
    #     return get_bad_request_key_response("point")


class TopListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    pagination_class = DiscoverListPagination

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        point = self.request.query_params.get('point', None)

        if point is not None and "," in point:
            point_array = point.split(",")
            lat = float(point_array[0])
            lng = float(point_array[1])
            city, district, locality = get_city_district_from_latlng(lng, lat)
            if city is not None:
                cache_key = "city_code_weekly_top_product_list_{}".format(city.code)

                try:
                    cache_result = cache.get(cache_key)
                    return cache_result
                except CacheMiss:
                    pass

        return self.queryset.annotate(avg_rating=Avg('product_comment__rate')).order_by("-avg_rating")
