# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView

from app.decorators import check_auth
from apps.branch.serializers import BranchListSerializer
from apps.helper.responseProcess.ResponseHelper import *

from app.permissions import IsOwner
from apps.product.serializers import ProductListSerializer

User = get_user_model()

from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView

)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)

from .serializers import (
    UserCreateSerializer,
    UserLoginSerializer,
    UserDetailSerializer,
    UserListSerializer,
    UserUpdateSerializer
)


class UserCreateAPIView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = {"token": serializer.data["token"]}
            return Response(new_data, status=HTTP_200_OK)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserDetailAPIView(RetrieveAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()


class UserMeAPIView(RetrieveAPIView):
    model = User
    permission_classes = [IsOwner]
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        if not is_authenticated(self.request):
            return get_not_authenticated_response()

        return get_detail_successfully_response(UserDetailSerializer(request.user).data)


class MyFavoritesListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        return self.queryset.filter(product_fav_product__user_id=self.request.user.id).order_by(
            "-product_fav_product__fav_date")


class MyFollowingBranchListAPIView(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        return self.queryset.filter(branch_follow_branch__user_id=self.request.user.id).order_by(
            "-branch_follow_branch__created_at")


class UserListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserListSerializer
    queryset = User.objects.all()


class UserUpdateAPIView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    permission_classes = [IsOwner]
    serializer_class = UserUpdateSerializer

    def put(self, request, *args, **kwargs):
        if not is_authenticated(self.request):
            return get_not_authenticated_response()

        user = request.user
        image = user.image
        if bool(user.image) and os.path.isfile(image.path):
            os.remove(image.path)

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

    def patch(self, request, *args, **kwargs):
        user = self.get_object()
        data = request.data
        if user:
            mode = data.get("mode")
            if mode is not None:
                if mode == "update-email-permission":
                    permission = data.get("emailPermission")
                    if permission is not None:
                        user.is_allow_email_notification = permission
                        user.save()
                    else:
                        return get_bad_request_key_response("emailPermission")
                elif mode == "update-push-permission":
                    permission = data.get("pushPermission")
                    if permission is not None:
                        user.is_allow_push_notification = permission
                        user.save()
                    else:
                        return get_bad_request_key_response("pushPermission")

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
