# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.password_validation import validate_password
from django.core import exceptions as django_exceptions
from django.db import IntegrityError, transaction
from djoser.conf import settings

from apps.company.serializers import CompanyDetailSerializer, CompanyDetailWithoutUserSerializer
from apps.helper import mD5Generator, Base64ImageField
from django.db.models import Q

from rest_framework_jwt.settings import api_settings
from django.contrib.auth import get_user_model

from djoser.serializers import UserSerializer as CustomUserSerializer

from apps.phoneNumber.serializers import PhoneNumberListSerializer
from app.models import PhoneNumber, Company, Notification

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

from rest_framework import serializers
from djoser.serializers import UserCreateSerializer as BaseUserRegistrationSerializer
from apps.helper.Base64ImageField import Base64ImageField

from rest_framework.serializers import (
    CharField,
    EmailField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError
)

User = get_user_model()


class UserRegistrationSerializer(BaseUserRegistrationSerializer):
    class Meta(BaseUserRegistrationSerializer.Meta):
        fields = ('email', 'username', 'password', 'user_type')

    def validate(self, attrs):
        attrs.pop("client_type", None)
        user = User(**attrs)
        password = attrs.get('password')

        try:
            validate_password(password, user)
        except django_exceptions.ValidationError as e:
            serializer_error = serializers.as_serializer_error(e)
            raise serializers.ValidationError({
                'password': serializer_error['non_field_errors']
            })

        return attrs

    def create(self, validated_data):
        try:
            user = self.perform_create(validated_data)
        except IntegrityError:
            self.fail('cannot_create_user')

        client_type = self.context.get("request").data.get("client_type")

        if client_type is not None:
            user_type = -1

            if client_type == 'MOBILE':
                user_type = 3
            elif client_type == 'WEB':
                user_type = 2
                user.is_corporate_user = True

            if user_type != -1:
                user.user_type = user_type
                user.save()

        return user

    def perform_create(self, validated_data):
        with transaction.atomic():
            user = User.objects.create_user(**validated_data)
            if settings.SEND_ACTIVATION_EMAIL:
                user.is_active = False
                user.save(update_fields=['is_active'])
        return user


class UserCreateSerializer(ModelSerializer):
    email = EmailField(label='Email Address')
    email2 = EmailField(label='Confirm Email')

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'email',
            'email2'
        ]
        extra_kwargs = {'password':
                            {'write_only': True}
                        }

    def validate(self, data):
        email = data['email']
        username = data['username']

        user_qs = User.objects.filter(email=email)
        user_by_name = User.objects.filter(username=username)
        if user_qs.exists():
            raise ValidationError("This user has already registered.")
        elif user_by_name.exists():
            raise ValidationError("This username has already taken.")
        return data

    def validate_email2(self, value):
        data = self.get_initial()
        email1 = data.get('email')
        email2 = value
        if email1 != email2:
            raise ValidationError("Emails must match")
        return value

    def create(self, validated_data):
        username = validated_data["username"]
        email = validated_data['email']
        password = validated_data['password']
        register_token = mD5Generator.generateMD5(email)

        user_obj = User()
        user_obj.password = password
        user_obj.username = username
        user_obj.email = email
        user_obj.register_token = register_token
        user_obj.is_staff = True
        user_obj.is_superuser = True
        user_obj.save()

        return validated_data


class UserLoginSerializer(ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    username = CharField(allow_blank=True, required=False)
    email = EmailField(allow_blank=True, required=False, label='Email Address')

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'email',
            'token'
        ]
        extra_kwargs = {'password':
                            {'write_only': True},
                        'username':
                            {'write_only': True},
                        'email':
                            {'write_only': True}
                        }

    def validate(self, data):
        email = data.get("email", None)
        username = data.get("username", None)
        password = data["password"]
        if not email and not username:
            raise ValidationError("Username or email is required to login.")
        user = User.objects.filter(
            Q(email=email) |
            Q(username=username)
        ).distinct()

        user = user.exclude(email__isnull=True).exclude(email__iexact=' ').exclude(email__iexact='')

        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("This username or email is not valid.")

        if user_obj:
            if not user_obj.password == password:
                raise ValidationError("Incorrect password")

        return data


class UserDetailSerializer(CustomUserSerializer):
    id = serializers.ReadOnlyField()
    phone_number = SerializerMethodField()
    company = SerializerMethodField()
    notification_count = SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'status',
            'is_staff',
            'is_active',
            'user_type',
            'email',
            'name_surname',
            'phone_number',
            'birthday',
            'image',
            'is_allow_email_notification',
            'is_superuser',
            'is_allow_push_notification',
            'company',
            'notification_count'
        ]

    def get_phone_number(self, obj):
        c_qs = PhoneNumber.objects.filter(user_id=obj.id).first()
        if c_qs is None:
            return None
        phone_number = PhoneNumberListSerializer(c_qs, many=False).data
        return phone_number

    def get_company(self, obj):
        company = Company.objects.filter(user_id=obj.id).first()
        if company is None:
            return None
        company_json = CompanyDetailWithoutUserSerializer(company).data
        return company_json

    def get_notification_count(self, obj):
        return Notification.objects.filter(user=obj.id, is_read=False).count()


class UserListSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'name_surname',
            'image',
            'email',
            'user_type',
            'is_staff',
            'is_superuser',
            'is_active',
        ]


class UserUpdateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True, allow_null=True, required=False
    )

    class Meta:
        model = User
        fields = [
            'username',
            'name_surname',
            'image',
            'birthday'

        ]
