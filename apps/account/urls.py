# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.account.views import (
    UserCreateAPIView,
    UserLoginAPIView,
    UserDetailAPIView,
    UserMeAPIView,
    UserListAPIView,
    UserUpdateAPIView,
    MyFavoritesListAPIView,
    MyFollowingBranchListAPIView
)

urlpatterns = [
    url(r'^register/$', UserCreateAPIView.as_view(), name='api-user-register'),
    url(r'^login/$', UserLoginAPIView.as_view(), name='api-user-login'),
    url(r'^detail/(?P<pk>\d+)', UserDetailAPIView.as_view(), name='api-user-detail'),
    url(r'^me/$', UserMeAPIView.as_view(), name='api-user-me'),
    url(r'^list/$', UserListAPIView.as_view(), name='api-user-list'),
    url(r'^my-favorites/$', MyFavoritesListAPIView.as_view(), name='api-my-favorites-list'),
    url(r'^my-following-branches/$', MyFollowingBranchListAPIView.as_view(), name='api-my-following-branches-list'),
    url(r'^edit/(?P<pk>\d+)', UserUpdateAPIView.as_view(), name='api-user-edit')
]
