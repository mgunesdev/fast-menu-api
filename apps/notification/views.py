# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)
from rest_framework.views import APIView

from app.models import Notification
from .serializers import (
    NotificationSerializer
)


class NotificationCreateAPIView(CreateAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class NotificationListAPIView(ListAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        current_set = self.queryset

        if self.request.user:
            return current_set.filter(user_id=self.request.user.id)

        return current_set


class NotificationDetailAPIView(RetrieveAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Notification.objects.filter(id=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class NotificationUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class NotificationDeleteAPIView(DestroyAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        self.get_object().delete()

        return get_deleted_successfully_response()


class ReadAllNotificationsAPIView(APIView):

    @check_auth
    def post(self, request, format=None):
        if self.request.user:
            notifications = Notification.objects.filter(user_id=self.request.user.id, is_read=False)
            for notification in notifications:
                notification.is_read = True
                notification.save()

        return get_updated_successfully_response(None)
