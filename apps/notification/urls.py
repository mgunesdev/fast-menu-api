# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.notification.views import (
    NotificationCreateAPIView,
    NotificationListAPIView,
    NotificationDetailAPIView,
    NotificationDeleteAPIView,
    NotificationUpdateAPIView,
    ReadAllNotificationsAPIView
)

urlpatterns = [
    url(r'^create/$', NotificationCreateAPIView.as_view(), name='api-notification-create'),
    url(r'^detail/(?P<pk>\d+)', NotificationDetailAPIView.as_view(), name='api-notification-detail'),
    url(r'^delete/(?P<pk>\d+)', NotificationDeleteAPIView.as_view(), name='api-notification-delete'),
    url(r'^list/$', NotificationListAPIView.as_view(), name='api-notification-list'),
    url(r'^read-all/$', ReadAllNotificationsAPIView.as_view(), name='api-notification-read-all'),
    url(r'^edit/(?P<pk>\d+)', NotificationUpdateAPIView.as_view(), name='api-notification-edit')

]
