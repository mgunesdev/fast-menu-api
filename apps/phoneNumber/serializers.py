# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer
from app.models import PhoneNumber


class PhoneNumberCreateSerializer(ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = [
            'id',
            'user',
            'country_code',
            'phone'
        ]


class PhoneNumberListSerializer(ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = [
            'id',
            'user',
            'country_code',
            'phone'
        ]


class PhoneNumberUpdateSerializer(ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = [
            'id',
            'country_code',
            'phone'
        ]


class PhoneNumberDeleteSerializer(ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = ['id']
