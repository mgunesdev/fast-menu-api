# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.models import PhoneNumber
from .serializers import (
    PhoneNumberCreateSerializer,
    PhoneNumberListSerializer,
    PhoneNumberDeleteSerializer,
    PhoneNumberUpdateSerializer
)


class PhoneNumberCreateAPIView(CreateAPIView):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class PhoneNumberListAPIView(ListAPIView):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class PhoneNumberDeleteAPIView(DestroyAPIView):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            phone_number = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, phone_number):
            return get_not_owner_response()

        phone_number.delete()

        return get_deleted_successfully_response()


class PhoneNumberUpdateAPIView(RetrieveUpdateAPIView):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            phone_number = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, phone_number):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)
