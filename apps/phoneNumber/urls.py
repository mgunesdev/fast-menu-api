# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.phoneNumber.views import (
    PhoneNumberCreateAPIView,
    PhoneNumberListAPIView,
    PhoneNumberDeleteAPIView,
    PhoneNumberUpdateAPIView
)

urlpatterns = [
    url(r'^list/$', PhoneNumberListAPIView.as_view(), name='api-phone_number-list'),
    url(r'^delete/(?P<pk>\d+)', PhoneNumberDeleteAPIView.as_view(), name='api-phone_number-delete'),
    url(r'^create/$', PhoneNumberCreateAPIView.as_view(), name='api-phone_number-create'),
    url(r'^edit/(?P<pk>\d+)', PhoneNumberUpdateAPIView.as_view(), name='api-phone_number-edit')
]
