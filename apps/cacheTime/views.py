# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
import django_filters.rest_framework
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    ListAPIView
)

from app.models import Campaign, Notification, CacheTime
from apps.helper.serializers.dynamic_fields import DynamicFieldsViewMixin
from .serializers import (
    CacheTimeSerializer
)


class CacheTimeListAPIView(ListAPIView):
    queryset = CacheTime.objects.all()
    serializer_class = CacheTimeSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['is_active', 'type']

    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)
