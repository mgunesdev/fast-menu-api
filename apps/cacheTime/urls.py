# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.cacheTime.views import (
    CacheTimeListAPIView)

urlpatterns = [
    url(r'^list/', CacheTimeListAPIView.as_view(), name='api-cache-time-list'),
]
