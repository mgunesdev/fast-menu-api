# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Campaign, CacheTime


class CacheTimeSerializer(ModelSerializer):
    class Meta:
        model = CacheTime
        fields = '__all__'
