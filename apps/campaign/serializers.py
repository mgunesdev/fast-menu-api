# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Campaign
from apps.helper.Base64ImageField import Base64ImageField


class CampaignSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, allow_empty_file=True, use_url=True, required=False
    )

    class Meta:
        model = Campaign
        fields = ["product",
                  "id",
                  "image",
                  "discount_rate",
                  "start_date",
                  "end_date"]
