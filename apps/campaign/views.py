# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
from apps.helper.ApiViewHelper import checkDataGetSerializer
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.models import Campaign, Notification
from .serializers import (
    CampaignSerializer
)


class CampaignCreateAPIView(CreateAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            try:
                checkDataGetSerializer(self, request.data)
            except ValidationError as exp:
                return get_bad_request_response(exp)

            if not check_owner_for_model(request, Product.objects.get(id=request.data["product"])):
                return get_not_owner_response()

            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CampaignListAPIView(ListAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data["results"])


class CampaignDetailAPIView(RetrieveAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Campaign.objects.filter(id=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class CampaignUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CampaignDeleteAPIView(DestroyAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            self.get_object()
        except Exception:
            return get_not_found_response()

        self.get_object().delete()

        return get_deleted_successfully_response()
