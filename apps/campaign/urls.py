# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.campaign.views import (
    CampaignCreateAPIView,
    CampaignListAPIView,
    CampaignDetailAPIView,
    CampaignDeleteAPIView,
    CampaignUpdateAPIView
)

urlpatterns = [
    url(r'^create/$', CampaignCreateAPIView.as_view(), name='api-campaign-create'),
    url(r'^detail/(?P<pk>\d+)', CampaignDetailAPIView.as_view(), name='api-campaign-detail'),
    url(r'^delete/(?P<pk>\d+)', CampaignDeleteAPIView.as_view(), name='api-campaign-delete'),
    url(r'^list/$', CampaignListAPIView.as_view(), name='api-campaign-list'),
    url(r'^edit/(?P<pk>\d+)', CampaignUpdateAPIView.as_view(), name='api-campaign-edit')

]
