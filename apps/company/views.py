# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from apps.helper.ApiViewHelper import checkDataGetSerializer
from app.models import Company, CompanyImage

from .serializers import (
    CompanyCreateSerializer,
    CompanyListSerializer,
    CompanyDetailSerializer,
    CompanyUpdateSerializer,
    CompanyDeleteSerializer
)


class CompanyCreateAPIView(CreateAPIView):
    queryset = Company
    serializer_class = CompanyCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            result = self.create(request, *args, **kwargs)
            company = Company.objects.get(id=result.data.get("id"))
            return get_created_successfully_response(CompanyListSerializer(company).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CompanyListAPIView(ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class CompanyDetailAPIView(RetrieveAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyDetailSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        company = Company.objects.filter(id=kwargs["pk"]).first()
        if not check_owner_for_model(request, company):
            return get_not_owner_response()

        if company is None:
            return get_not_found_response()
        return get_detail_successfully_response(CompanyDetailSerializer(company).data)


class MyCompanyAPIView(RetrieveAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyDetailSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        current_set = self.queryset
        if request.user.parent is None:
            current_set = current_set.get(user_id=request.user.id)
        else:
            current_set = current_set.get(user_id=request.user.parent.id)

        return get_detail_successfully_response(CompanyDetailSerializer(current_set).data)


class CompanyUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            company = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, company):
            return get_not_owner_response()

        if request.data.get("logo"):
            logo = company.logo
            if os.path.isfile(logo.path):
                os.remove(logo.path)

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

    @check_auth
    def patch(self, request, *args, **kwargs):
        try:
            company = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, company):
            return get_not_owner_response()

        try:
            mode = request.data["mode"]
        except Exception:
            return get_bad_request_key_response("mode")

        if mode == "add-images":
            new_images = request.data["new-images"]
            for image_id in new_images:
                company_image = CompanyImage.objects.filter(id=image_id).first()
                company_image.company = self.get_object()
                company_image.save()
        elif mode == "change-orders":
            new_orders = request.data["new-orders"]
            for id in new_orders:
                int_id = int(id)
                order = new_orders[id]
                company_image = CompanyImage.objects.filter(id=int_id).first()
                company_image.order = order
                company_image.save()

        elif mode == "delete-image":
            imageId = request.data["image-id"]
            imageList = list(company.company_image.all())
            delImage = CompanyImage.objects.filter(id=imageId)
            delImage = delImage.first()
            index = imageList.index(delImage)
            imageList.remove(delImage)

            for i in range(index, len(imageList)):
                company_image = imageList[i]
                if company_image.order == 1:
                    break
                company_image.order = company_image.order - 1
                company_image.save()

            delImage.delete()

            company.save()

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CompanyDeleteAPIView(DestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            company = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, company):
            return get_not_owner_response()

        if company.logo:
            if os.path.isfile(company.logo.path):
                os.remove(company.logo.path)

        for company_image in CompanyImage.objects.filter(company_id=company.id):
            if os.path.isfile(company_image.image.path):
                os.remove(company_image.image.path)

        company.delete()

        return get_deleted_successfully_response()
