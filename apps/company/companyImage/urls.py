# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.company.companyImage.views import \
    (CompanyImageCreateAPIView,
     CompanyImageListAPIView,
     CompanyImageDeleteAPIView)

urlpatterns = [
    url(r'^create/$', CompanyImageCreateAPIView.as_view(), name='api-companyimage-create'),
    url(r'^list/$', CompanyImageListAPIView.as_view(), name='api-companyimage-list'),
    url(r'^delete/(?P<pk>\d+)', CompanyImageDeleteAPIView.as_view(), name='api-companyimage-delete')
]
