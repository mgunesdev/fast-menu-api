# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from apps.helper.Base64ImageField import Base64ImageField
from app.models import CompanyImage


class CompanyImageCreateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, allow_empty_file=True, use_url=True,
    )

    class Meta:
        model = CompanyImage
        fields = ['id', 'company', 'image']


class CompanyImageSerializer(ModelSerializer):
    class Meta:
        model = CompanyImage
        fields = ['id', 'image', 'order']


class CompanyImageDeleteSerializer(ModelSerializer):
    class Meta:
        model = CompanyImage
        fields = ['id']
