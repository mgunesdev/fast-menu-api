# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SerializerMethodField

from apps.company.companyImage.serializers import CompanyImageSerializer
from apps.helper.Base64ImageField import Base64ImageField
from apps.helper.responseProcess.ModelResponseHelper import get_short_user_model
from app.models import Company
from apps.location.serializers import LocationSerializer


class CompanyCreateSerializer(ModelSerializer):
    logo = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Company
        fields = ['id',
                  'user',
                  'name',
                  'description',
                  'logo',
                  'location'
                  ]


class CompanyListSerializer(ModelSerializer):
    user = SerializerMethodField()
    location = LocationSerializer()

    class Meta:
        model = Company
        fields = ['id',
                  'user',
                  'name',
                  'logo',
                  'location'
                  ]

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class CompanyDetailSerializer(ModelSerializer):
    images = CompanyImageSerializer(source="company_image", many=True, read_only=True)
    user = SerializerMethodField()
    location = LocationSerializer()

    class Meta:
        model = Company
        fields = ['id',
                  'user',
                  'name',
                  'description',
                  'logo',
                  'location',
                  'created_at',
                  'updated_at',
                  'images'
                  ]

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class CompanyDetailWithoutUserSerializer(ModelSerializer):
    images = CompanyImageSerializer(source="company_image", many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['id',
                  'name',
                  'description',
                  'logo',
                  'location',
                  'created_at',
                  'updated_at',
                  'images'
                  ]


class CompanyUpdateSerializer(ModelSerializer):
    logo = Base64ImageField(
        max_length=None, use_url=True, allow_null=True, required=False
    )

    class Meta:
        model = Company
        fields = ['id',
                  'name',
                  'description',
                  'logo',
                  'location'
                  ]


class CompanyDeleteSerializer(ModelSerializer):
    class Meta:
        model = Company
        fields = ['id']
