# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.urls import include

from apps.company.views import (
    CompanyCreateAPIView,
    CompanyListAPIView,
    CompanyDeleteAPIView,
    CompanyUpdateAPIView,
    CompanyDetailAPIView,
    MyCompanyAPIView

)

urlpatterns = [
    url(r'^create/$', CompanyCreateAPIView.as_view(), name='api-company-create'),
    url(r'^detail/(?P<pk>\d+)', CompanyDetailAPIView.as_view(), name='api-company-detail'),
    url(r'^my-company/$', MyCompanyAPIView.as_view(), name='api-my-company'),
    url(r'^delete/(?P<pk>\d+)', CompanyDeleteAPIView.as_view(), name='api-company-delete'),
    url(r'^list/$', CompanyListAPIView.as_view(), name='api-company-list'),
    url(r'^edit/(?P<pk>\d+)', CompanyUpdateAPIView.as_view(), name='api-company-edit'),
    url(r'^image/', include('apps.company.companyImage.urls'))

]
