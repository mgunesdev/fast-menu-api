# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Ingredient


class IngredientSerializer(ModelSerializer):
    class Meta:
        model = Ingredient
        fields = [
            'id',
            'name'
        ]


class IngredientDeleteSerializer(ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ['id']
