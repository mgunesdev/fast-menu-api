# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.exceptions import ValidationError

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView,
    RetrieveAPIView
)

from app.models import Ingredient

from .serializers import (
    IngredientSerializer,
    IngredientDeleteSerializer
)


class IngredientCreateAPIView(CreateAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    @check_auth
    def post(self, request, *args, **kwargs):

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class IngredientListAPIView(ListAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword', None)
        queryset = self.queryset
        if keyword is not None:
            queryset = queryset.filter(name__icontains=keyword)

        return queryset


class IngredientUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            ingredient = self.get_object()
        except Exception:
            return get_not_found_response()
        #
        # if not check_owner_for_model(request, ingredient):
        #     return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class IngredientDetailAPIView(RetrieveAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        ingredient = Ingredient.objects.filter(id=kwargs["pk"]).first()
        if ingredient is None:
            return get_not_found_response()
        return get_detail_successfully_response(IngredientSerializer(ingredient).data)


class IngredientDeleteAPIView(DestroyAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            ingredient = self.get_object()
        except Exception:
            return get_not_found_response()
        #
        # if not is_owner(request, ingredient):
        #     return get_not_owner_response()

        ingredient.delete()

        return get_deleted_successfully_response()
