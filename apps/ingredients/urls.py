# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.ingredients.views import (
    IngredientCreateAPIView,
    IngredientListAPIView,
    IngredientDetailAPIView,
    IngredientDeleteAPIView,
    IngredientUpdateAPIView
)

urlpatterns = [
    url(r'^create/$', IngredientCreateAPIView.as_view(), name='api-ingredient-create'),
    url(r'^detail/(?P<pk>\d+)', IngredientDetailAPIView.as_view(), name='api-ingredient-detail'),
    url(r'^delete/(?P<pk>\d+)', IngredientDeleteAPIView.as_view(), name='api-ingredient-delete'),
    url(r'^list/$', IngredientListAPIView.as_view(), name='api-ingredient-list'),
    url(r'^edit/(?P<pk>\d+)', IngredientUpdateAPIView.as_view(), name='api-ingredient-edit')
]
