# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import Feedback
from apps.helper.responseProcess.ModelResponseHelper import get_short_user_model


class FeedbackCreateSerializer(ModelSerializer):
    class Meta:
        model = Feedback
        fields = ['id',
                  'user',
                  'message',
                  'created_at']


class FeedbackSerializer(ModelSerializer):
    user = SerializerMethodField()

    class Meta:
        model = Feedback
        fields = ['id',
                  'user',
                  'message',
                  'created_at']

    def get_user(self, obj):
        return get_short_user_model(self, obj.user)


class FeedbackDeleteSerializer(ModelSerializer):
    class Meta:
        model = Feedback
        fields = ['id']
