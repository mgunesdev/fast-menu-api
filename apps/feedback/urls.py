# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.feedback.views import (
    FeedbackCreateAPIView,
    FeedbackListAPIView,
    FeedbackDetailAPIView,
    FeedbackDeleteAPIView
)

urlpatterns = [
    url(r'^create/$', FeedbackCreateAPIView.as_view(), name='api-feedback-create'),
    url(r'^detail/(?P<pk>\d+)', FeedbackDetailAPIView.as_view(), name='api-feedback-detail'),
    url(r'^delete/(?P<pk>\d+)', FeedbackDeleteAPIView.as_view(), name='api-feedback-delete'),
    url(r'^list/$', FeedbackListAPIView.as_view(), name='api-feedback-list')
]
