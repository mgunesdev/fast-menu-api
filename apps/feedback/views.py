# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
)

from app.models import Feedback
from .serializers import (
    FeedbackCreateSerializer,
    FeedbackSerializer,
    FeedbackDeleteSerializer
)


class FeedbackCreateAPIView(CreateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class FeedbackListAPIView(ListAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class FeedbackDetailAPIView(RetrieveAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        if Feedback.objects.filter(id=kwargs["pk"]).first() is None:
            return get_not_found_response()
        return get_detail_successfully_response(self.retrieve(request, *args, **kwargs).data)


class FeedbackDeleteAPIView(DestroyAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            feedback = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, feedback):
            return get_not_owner_response()

        feedback.delete()

        return get_deleted_successfully_response()
