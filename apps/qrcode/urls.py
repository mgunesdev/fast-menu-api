# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.qrcode.views import (
    QrCodeCreateAPIView,
    QrCodeListAPIView,
    QrCodeDeleteAPIView,
    QrCodeReadCreateAPIView
)

urlpatterns = [
    url(r'^create/$', QrCodeCreateAPIView.as_view(), name='api-code-create'),
    url(r'^delete/(?P<pk>\d+)', QrCodeDeleteAPIView.as_view(), name='api-code-delete'),
    url(r'^list/$', QrCodeListAPIView.as_view(), name='api-code-list'),
    url(r'^read/$', QrCodeReadCreateAPIView.as_view(), name='api-code-read')
]
