# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

from rest_framework.exceptions import ValidationError

from app.decorators import check_menu_availability, check_auth
from apps.helper.Base64ImageField import Base64ImageField

from apps.helper.polygonChecker import get_distance_between_two_loc_point
from apps.helper.responseProcess.ResponseHelper import *

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
)
from apps.helper.ApiViewHelper import checkDataGetSerializer

from app.models import QrCode, QrCodeRead, Menu
from django.contrib.auth.models import AnonymousUser
import pyqrcode

from apps.menu.serializers import MenuModelDetailSerializer
from .serializers import (
    QrCodeCreateSerializer,
    QrCodeSerializer,
    QrCodeDeleteSerializer,
    QrCodeReadCreateSerializer
)


class QrCodeCreateAPIView(CreateAPIView):
    queryset = QrCode.objects.all()
    serializer_class = QrCodeCreateSerializer

    @check_auth
    @check_menu_availability
    def post(self, request, *args, **kwargs):
        data = request.data

        try:
            serializer = checkDataGetSerializer(self, data)
            headers = self.get_success_headers(serializer.data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        import environ

        env = environ.Env(
            # set casting, default value
            DEBUG=(bool, False)
        )
        # reading .env file
        environ.Env.read_env()
        new_code = QrCode()

        menu = request.data["menu_instance"]
        new_code.menu = menu
        new_code.save()

        content = "{}/menu-{}-qr{}".format(env('STATIC_URL'), menu.id, new_code.id)

        new_code.content = content

        qr_png = pyqrcode.create(content)

        new_code.image = Base64ImageField().to_internal_value(qr_png.png_as_base64_str(scale=5))
        new_code.image.name = "company_{}/branch_{}_menu_{}_qr.png".format(menu.branch.company.id, menu.branch.id,
                                                                           menu.id)
        new_code.save()
        return get_created_successfully_response(QrCodeSerializer(new_code).data)


class QrCodeReadCreateAPIView(CreateAPIView):
    queryset = QrCodeRead.objects.all()
    serializer_class = QrCodeReadCreateSerializer

    def post(self, request, *args, **kwargs):
        data = request.data

        try:
            checkDataGetSerializer(self, data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

        menu = Menu.objects.get(id=request.data["menu"])
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        qr_code = QrCode.objects.get(id=request.data["qr_code"])
        branch_location = menu.branch.location
        if branch_location:
            distance = get_distance_between_two_loc_point(latitude, longitude, branch_location.latitude,
                                                          branch_location.longitude)
            if distance * 1000 < 50:
                try:
                    new_code_read = QrCodeRead()
                    if type(request.user) is not AnonymousUser:
                        new_code_read.user = request.user

                    new_code_read.menu = menu
                    new_code_read.latitude = latitude
                    new_code_read.longitude = longitude
                    new_code_read.qr_code = qr_code
                    new_code_read.save()
                    return get_detail_successfully_response(
                        MenuModelDetailSerializer(menu, context={'request': request}).data)

                except Exception:
                    print("outside", distance)

                    get_unknown_error_response()
            else:
                return get_bad_request_message_response("QrCode ile menüyü görmek için çok uzaktasınız! ")

        return get_unknown_error_response()


class QrCodeListAPIView(ListAPIView):
    queryset = QrCode.objects.all()
    serializer_class = QrCodeCreateSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(menu__branch__user=request.user.id)

        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class QrCodeDeleteAPIView(DestroyAPIView):
    queryset = QrCode.objects.all()
    serializer_class = QrCodeDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            code = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, code.menu):
            return get_not_owner_response()

        if os.path.isfile(code.image.path):
            os.remove(code.image.path)

        code.delete()

        return get_deleted_successfully_response()
