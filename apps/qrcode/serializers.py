# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from app.models import QrCode, QrCodeRead


class QrCodeCreateSerializer(ModelSerializer):
    class Meta:
        model = QrCode
        fields = [
            'id',
            'menu'
        ]


class QrCodeReadCreateSerializer(ModelSerializer):
    class Meta:
        model = QrCodeRead
        fields = '__all__'


class QrCodeSerializer(ModelSerializer):
    class Meta:
        model = QrCode
        fields = '__all__'


class QrCodeMenuListSerializer(ModelSerializer):
    class Meta:
        model = QrCode
        fields = ['image']


class QrCodeDeleteSerializer(ModelSerializer):
    class Meta:
        model = QrCode
        fields = ['id']
