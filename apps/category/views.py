# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from app.decorators import check_auth
from apps.helper.responseProcess.ResponseHelper import *
from rest_framework.exceptions import ValidationError

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveUpdateAPIView
)

from app.models import Category, Product
from .serializers import (
    CategoryCreateSerializer,
    CategoryDetailSerializer,
    CategoryListSerializer,
    CategoryUpdateSerializer,
    CategoryDeleteSerializer
)


class CategoryCreateAPIView(CreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer

    @check_auth
    def post(self, request, *args, **kwargs):
        if request.data.get("menu"):

            if not check_owner_for_model(request, Menu.objects.get(id=request.data["menu"])):
                return get_not_owner_response()

        try:
            return get_created_successfully_response(self.create(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CategoryListAPIView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)


class MyCategoryList(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        return get_list_successfully_response(request.GET, self.list(request, *args, **kwargs).data)

    def get_queryset(self):
        current_set = self.queryset
        params = self.request.query_params
        branch_id = params.get('branch', None)
        menu_id = params.get('menu', None)
        status = params.get('status', None)

        if status is not None:
            current_set = current_set.filter(status=status)

        if branch_id is not None:
            current_set = current_set.filter(menu__branch_id=branch_id)

        if menu_id is not None:
            current_set = current_set.filter(menu_id=menu_id)

        if self.request.user.parent is None:
            return current_set.filter(menu__branch__company__user_id=self.request.user.id)
        else:
            return current_set.filter(menu__branch__user_id=self.request.user.id)


class CategoryDetailAPIView(RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryDetailSerializer

    @check_auth
    def get(self, request, *args, **kwargs):
        category = Category.objects.filter(id=kwargs["pk"]).first()
        if category is None:
            return get_not_found_response()
        return get_detail_successfully_response(CategoryDetailSerializer(category, context={'request': request}).data)


class CategoryUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryUpdateSerializer

    @check_auth
    def put(self, request, *args, **kwargs):
        try:
            menu = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, menu):
            return get_not_owner_response()

        try:
            return get_updated_successfully_response(self.update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)

    @check_auth
    def patch(self, request, *args, **kwargs):
        try:
            category = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, category):
            return get_not_owner_response()

        data = request.data
        if category:
            mode = data.get("mode")
            if mode is not None:
                if mode == "change-product-sort-order":
                    newOrders = request.data["newOrders"]
                    product_list = Product.objects.filter(category_id=category.id)
                    if newOrders is not None:
                        print(type(newOrders))
                        if len(product_list) == len(newOrders):
                            for product in product_list:
                                product.order = newOrders[str(product.id)]
                                product.save()
                        else:
                            return get_bad_request_message_response("Ürünler ile sıra dizisi uyuşmamaktadır.")
                    else:
                        return get_bad_request_key_response("newOrders")
                elif mode == "update-status":
                    status = data.get("status")
                    if status is not None:
                        category.status = status
                        category.save()
                    else:
                        return get_bad_request_key_response("status")
                else:
                    return get_bad_request_message_response("Mode hatası")

        try:
            return get_updated_successfully_response(self.partial_update(request, *args, **kwargs).data)
        except ValidationError as exp:
            return get_bad_request_response(exp)


class CategoryDeleteAPIView(DestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryDeleteSerializer

    @check_auth
    def destroy(self, request, *args, **kwargs):
        try:
            category = self.get_object()
        except Exception:
            return get_not_found_response()

        if not check_owner_for_model(request, category):
            return get_not_owner_response()

        if os.path.isfile(category.image.path):
            os.remove(category.image.path)

        menu = category.menu
        if os.path.isfile(category.image.path):
            os.remove(category.image.path)
        category.delete()

        category_list = Category.objects.filter(menu_id=menu.id)
        for index in range(len(category_list)):
            remaining_category = category_list[index]
            remaining_category.order = index + 1
            remaining_category.save()

        return get_deleted_successfully_response()
