# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.helper.Base64ImageField import Base64ImageField
from apps.product.serializers import ProductDetailSerializer
from app.models import Category, Product


class CategoryCreateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Category
        fields = ['id',
                  'menu',
                  'title',
                  'image',
                  'order',
                  'status'
                  ]


class CategoryListSerializer(ModelSerializer):
    product_count = SerializerMethodField()

    class Meta:
        model = Category
        fields = ['id',
                  'menu',
                  'title',
                  'image',
                  'order',
                  'product_count',
                  'status'
                  ]

    def get_product_count(self, obj):
        c_qs = Product.objects.filter(category_id=obj.id)
        return len(c_qs)


class CategoryDetailSerializer(ModelSerializer):
    product_count = SerializerMethodField()
    products = SerializerMethodField()

    class Meta:
        model = Category
        fields = [
            'id',
            'menu',
            'title',
            'image',
            'order',
            'product_count',
            'products',
            'status'
        ]

    def get_product_count(self, obj):
        c_qs = Product.objects.filter(category_id=obj.id)
        return len(c_qs)

    def get_products(self, obj):
        c_qs = Product.objects.filter(category_id=obj.id)
        products = ProductDetailSerializer(c_qs, many=True, context={'request': self.context['request']}).data
        return products


class CategoryUpdateSerializer(ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True, allow_null=True, required=False
    )

    class Meta:
        model = Category
        fields = ['id',
                  'title',
                  'image',
                  'order',
                  'status'
                  ]


class CategoryDeleteSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['id']
