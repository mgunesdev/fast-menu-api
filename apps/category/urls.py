# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from apps.category.views import (
    CategoryCreateAPIView,
    CategoryListAPIView,
    CategoryDetailAPIView,
    CategoryUpdateAPIView,
    CategoryDeleteAPIView,
    MyCategoryList
)

urlpatterns = [
    url(r'^create/$', CategoryCreateAPIView.as_view(), name='api-category-create'),
    url(r'^detail/(?P<pk>\d+)', CategoryDetailAPIView.as_view(), name='api-category-detail'),
    url(r'^delete/(?P<pk>\d+)', CategoryDeleteAPIView.as_view(), name='api-category-delete'),
    url(r'^list/$', CategoryListAPIView.as_view(), name='api-category-list'),
    url(r'^my-category/$', MyCategoryList.as_view(), name='api-my-category-list'),
    url(r'^edit/(?P<pk>\d+)', CategoryUpdateAPIView.as_view(), name='api-category-edit')
]
