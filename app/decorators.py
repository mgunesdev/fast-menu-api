from app.models import (
    Menu,
    Branch,
    Company)

from apps.helper.responseProcess.ResponseHelper import (
    get_bad_request_message_response,
    get_not_authenticated_response
)


def check_menu_availability(func):
    def wrap(self, request, *args, **kwargs):
        if "menu" in request.data:
            try:
                menu = Menu.objects.get(
                    id=request.data["menu"],
                    branch__user__id=request.user.id
                )
                self.request.POST._mutable = True
                request.data['menu_instance'] = menu
                request.data['menu'] = menu.id
            except Menu.DoesNotExist:
                return get_bad_request_message_response("Geçersiz menü bilgisi!")
            except Exception:
                return get_bad_request_message_response("Geçersiz menü bilgisi!")
        else:
            return get_bad_request_message_response("Geçersiz menü bilgisi!")

        return func(self, request, *args, **kwargs)

    return wrap


def check_branch_availability(func):
    def wrap(self, request, *args, **kwargs):
        if "branch" in request.data:
            try:
                branch = Branch.objects.get(
                    id=request.data["branch"],
                    user__id=request.user.id
                )

                self.request.POST._mutable = True
                request.data['branch_instance'] = branch

            except Branch.DoesNotExist:
                return get_bad_request_message_response("Geçersiz branch bilgisi!")
            except Exception:
                return get_bad_request_message_response("Geçersiz branch bilgisi!")
        else:
            return get_bad_request_message_response("Geçersiz branch bilgisi!")

        return func(self, request, *args, **kwargs)

    return wrap


def check_company_availability(func):
    def wrap(self, request, *args, **kwargs):
        if "company" in request.data:
            try:
                branch = Company.objects.get(
                    id=request.data["company"],
                    user__id=request.user.id
                )

                self.request.POST._mutable = True
                request.data['company_instance'] = branch

            except Company.DoesNotExist:
                return get_bad_request_message_response("Geçersiz company bilgisi!")
            except Exception:
                return get_bad_request_message_response("Geçersiz company bilgisi!")
        else:
            return get_bad_request_message_response("Geçersiz company bilgisi!")

        return func(self, request, *args, **kwargs)

    return wrap


def check_auth(func):
    def wrap(self, request, *args, **kwargs):
        if not bool(request.user and request.user.is_authenticated):
            return get_not_authenticated_response()

        return func(self, request, *args, **kwargs)

    return wrap
