# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from cacheops import invalidate_model, invalidate_obj
from .models import (
    City,
    District,
    Location,
    Locality,
    Campaign, User, Notification, ProductImage)
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver


def invalidate_cache_objects(queryset):
    for obj in queryset.iterator():
        invalidate_obj(obj)


@receiver(post_save, sender=City)
def city_post_save(sender, instance=None, created=False, **kwargs):
    if not created:
        invalidate_cache_objects(City.objects.filter(code=instance.code))
        invalidate_cache_objects(District.objects.filter(parentcode=instance.code))


@receiver(post_save, sender=District)
def district_post_save(sender, instance=None, created=False, **kwargs):
    if not created:
        invalidate_cache_objects(District.objects.filter(code=instance.code))
        invalidate_cache_objects(Locality.objects.filter(parentcode=instance.code))


@receiver(post_save, sender=Locality)
def locality_post_save(sender, instance=None, created=False, **kwargs):
    if not created:
        invalidate_cache_objects(Locality.objects.filter(code=instance.code))


@receiver(post_save, sender=Campaign)
def campaign_post_save(instance, created, **kwargs):
    user_list = User.objects.filter(product_fav_user__product_id=instance.product.id, is_allow_push_notification=True)
    for user in user_list:
        notification = Notification()
        notification.user = user
        notification.routing_url = "http://www.gurme.kim/product/{}".format(instance.product.id)
        notification.routing_id = instance.product.id
        notification.type = Notification.TYPE_CAMPAIGN
        notification.type_id = instance.id
        notification.text = "{} mekanında {} ürününde %{} indirim. Bu fırsatı kaçırma! İndirimli fiyatı görmek için " \
                            "dokun!".format(
            instance.product.menu.branch.name, instance.product.title,
            instance.discount_rate)
        if instance.image:
            notification.image = instance.image
        else:
            notification.image = ProductImage.objects.filter(product_id=instance.product.id).first().image

        notification.save()
