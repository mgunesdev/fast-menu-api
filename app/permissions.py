from rest_framework.permissions import BasePermission, SAFE_METHODS
from app.models import Menu, Category, Product, User


class IsOwnerOrReadOnly(BasePermission):
    message = 'You must be the owner of this object'
    mySafeMethod = ['GET']

    def has_permission(self, request, view):
        if request.method in self.mySafeMethod:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        return obj.user == request.user


class IsStaffOrSuperUser(BasePermission):
    message = "You must be the staff user"

    def has_permission(self, request, view):
        return request.user.is_staff or request.user.is_superuser


class IsOwner(BasePermission):
    message = 'You must be the owner of this object'

    def has_object_permission(self, request, view, obj):
        if type(obj) is User:
            return obj == request.user
        return obj.user == request.user




# class IsOwnerOrParentOwner(BasePermission):
#     message = 'You must be the owner of this object'
#
#     def has_object_permission(self, request, view, obj):
#         instance_type = type(obj)
#         parent_obj = None
#         if instance_type is Menu:
#             parent_obj = obj.menu
#         elif instance_type is Category:
#             parent_obj = obj.category
#         elif instance_type is Product:
#             parent_obj = obj.product
#
#         return obj.user == request.user or parent_obj.user == request.user

        # import ipdb;
        # ipdb.set_trace()
