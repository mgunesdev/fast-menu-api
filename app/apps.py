# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import pre_save, post_save


class MenuAppConfig(AppConfig):
    name = 'app'

    def ready(self):
        from app.models import (
            City,
            District,
            Locality,
            Campaign
        )

        from app.signals import (
            city_post_save,
            district_post_save,
            locality_post_save,
            campaign_post_save

        )

        post_save.connect(city_post_save, sender=City)
        post_save.connect(district_post_save, sender=District)
        post_save.connect(locality_post_save, sender=Locality)
        post_save.connect(campaign_post_save, sender=Campaign)
