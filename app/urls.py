"""apps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from rest_framework_swagger.views import get_swagger_view

from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(r'devices', FCMDeviceAuthorizedViewSet)

schema_view = get_swagger_view(title='Gurme API')

urlpatterns = [
                  url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
                  url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
                  url(r'', admin.site.urls),
                  url(r'^', include(router.urls)),
                  url(r'^admin/', admin.site.urls),
                  url(r'^docs/', schema_view),
                  url(r'^auth/', include('djoser.urls')),
                  url(r'^auth/', include('djoser.urls.authtoken')),
                  url(r'^users/', include('apps.account.urls')),
                  url(r'^phones/', include('apps.phoneNumber.urls')),
                  url(r'^company/', include('apps.company.urls')),
                  url(r'^branch/', include('apps.branch.urls')),
                  url(r'^menu/', include('apps.menu.urls')),
                  url(r'^category/', include('apps.category.urls')),
                  url(r'^price/', include('apps.product.price.urls')),
                  url(r'^product/', include('apps.product.urls')),
                  url(r'^code/', include('apps.qrcode.urls')),
                  url(r'^feedback/', include('apps.feedback.urls')),
                  url(r'^city/', include('apps.location.city.urls')),
                  url(r'^district/', include('apps.location.district.urls')),
                  url(r'^locality/', include('apps.location.locality.urls')),
                  url(r'^location/', include('apps.location.urls')),
                  url(r'^property/', include('apps.property.urls')),
                  url(r'^discover/', include('apps.discover.urls')),
                  url(r'^search/', include('apps.search.urls')),
                  url(r'^ingredient/', include('apps.ingredients.urls')),
                  url(r'^discountcode/', include('apps.discountcode.urls')),
                  url(r'^discountcode-count/', include('apps.discountcode.count.urls')),
                  url(r'^campaign/', include('apps.campaign.urls')),
                  url(r'^cache/', include('apps.cacheTime.urls')),
                  url(r'^notification/', include('apps.notification.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
