from django.core.management.base import BaseCommand
from django.db.models import Count, Avg
from cacheops import cache, CacheMiss

from app.models import City, Product


class Command(BaseCommand):
    help = 'Weekly Top List Cache Process'

    def handle(self, *args, **kwargs):
        city_list = City.objects.all()
        for city in city_list:
            cache_key = "city_code_weekly_top_product_list_{}".format(city.code)
            query_result = Product.objects.annotate(avg_rating=Avg('product_comment__rate')).annotate(
                comment_count=Count('product_comment')).filter(
                comment_count__gt=20,
                menu__branch__location__city__code=city.code).order_by("-avg_rating")[:20]

            cache.set(cache_key, query_result, timeout=60 * 60 * 24 * 7)
