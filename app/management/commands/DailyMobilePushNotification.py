# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from fcm_django.models import FCMDevice
from datetime import datetime
from app.models import Notification


def getNotificationType(type):
    if type == Notification.TYPE_CAMPAIGN:
        return "open_campaign"
    elif type == Notification.TYPE_BRANCH:
        return "open_menu"


def getNotificationTitle(type):
    if type == Notification.TYPE_CAMPAIGN:
        return "Beğendiğin Üründe İndirim Var!"
    elif type == Notification.TYPE_BRANCH:
        return "Kaydettiğin menüye yeni bir ürün eklendi!"


class Command(BaseCommand):
    help = 'Daily Mobile Push Notification Process'

    def handle(self, *args, **kwargs):
        notifications = Notification.objects.filter(is_send=False)

        for notification in notifications:
            devices = FCMDevice.objects.filter(user_id=notification.user_id)

            devices.send_message(data={
                "title": getNotificationTitle(notification.type),
                "message": notification.text,
                "routing_url": notification.routing_url,
                "routing_id": notification.routing_id,
                "type": notification.type,
                "image": notification.image.url,
                "from_firebase": getNotificationType(notification.type)
            })

            notification.is_send = True
            notification.sending_at = datetime.now()
            notification.save()
