# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

admin.site.register(PhoneNumber)
admin.site.register(Menu)
admin.site.register(Category)
admin.site.register(PreparingTime)
admin.site.register(Product)
admin.site.register(Price)
admin.site.register(Ingredient)
admin.site.register(ProductIngredient)
admin.site.register(User)
admin.site.register(Company)
admin.site.register(BranchImage)
admin.site.register(CompanyImage)
admin.site.register(BranchPhoneNumber)
admin.site.register(Branch)
admin.site.register(Feedback)
admin.site.register(Campaign)
admin.site.register(Notification)
