# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.contrib.gis.geos import Point
from django.db import models
from django.contrib.gis.db import models as gmsmodel

from django.conf import settings
from django.contrib.postgres.fields import ArrayField

from django.contrib.auth.models import AbstractUser, UserManager
from django.db.models import Q

from apps.helper.pathcreater import get_file_path

# --- LOCATION SECTION STARTS ---
from django.dispatch import receiver


def upload_path_handler(instance, filename):
    return "user/images/menu/{id}/{ext}".format(id=instance.pk, ext=filename)


class City(models.Model):
    code = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=250)
    type = models.CharField(max_length=50)
    latitude = models.CharField(max_length=100)
    longitude = models.CharField(max_length=100)
    parentcode = models.IntegerField(null=True)
    polygon = ArrayField(ArrayField(models.FloatField()), null=True)
    point = gmsmodel.PointField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "Citys"
        db_table = "citys"
        ordering = ["name"]


class District(models.Model):
    code = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=250)
    slug = models.CharField(max_length=250)
    type = models.CharField(max_length=100)
    latitude = models.CharField(max_length=100)
    longitude = models.CharField(max_length=100)
    parentcode = models.IntegerField()
    polygon = ArrayField(ArrayField(models.FloatField()), null=True)
    point = gmsmodel.PointField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "Districts"
        db_table = "districts"
        ordering = ["name"]


class Locality(models.Model):
    code = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=250)
    slug = models.CharField(max_length=250)
    type = models.CharField(max_length=100)
    is_should_update = models.BooleanField(default=False)
    latitude = models.CharField(max_length=100, null=True)
    longitude = models.CharField(max_length=100, null=True)
    parentcode = models.IntegerField()
    polygon = ArrayField(ArrayField(models.FloatField()), null=True)
    point = gmsmodel.PointField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "Localities"
        db_table = "localities"
        ordering = ["name"]


class Location(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="location_user", on_delete=models.CASCADE,
                             null=True)
    locality = models.ForeignKey(Locality, related_name="location_locality", on_delete=models.SET_NULL,
                                 null=True)
    district = models.ForeignKey(District, related_name="location_district", on_delete=models.SET_NULL,
                                 null=True)
    city = models.ForeignKey(City, related_name="location_city", on_delete=models.SET_NULL,
                             null=True)

    address = models.TextField()
    latitude = models.CharField(max_length=100)
    longitude = models.CharField(max_length=100)
    point = gmsmodel.PointField(null=True, blank=True)

    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):
        return "{}".format(self.address)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        latitude = self.latitude
        longitude = self.longitude
        self.point = Point(float(latitude), float(longitude))
        super().save()

    class Meta:
        verbose_name_plural = "Locations"
        db_table = "locations"


class Property(models.Model):
    code = models.IntegerField(unique=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "Properties"
        db_table = "properties"
        ordering = ["code"]


class PhoneNumber(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="phone_number_user", on_delete=models.CASCADE,
                             null=True)
    country_code = models.CharField(max_length=5, null=True)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name_plural = "PhoneNumbers"
        db_table = "phone_numbers"


class CustomUserManager(UserManager):

    def get_by_natural_key(self, username):
        return self.get(
            Q(**{self.model.USERNAME_FIELD: username}) |
            Q(**{self.model.EMAIL_FIELD: username})
        )


class User(AbstractUser):
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=150, blank=True, null=True)
    status = models.SmallIntegerField(default=0)
    user_type = models.SmallIntegerField(default=1)
    email = models.EmailField(unique=True)
    is_verified = models.BooleanField(default=False)
    register_token = models.TextField(null=True, blank=True)
    refresh_token = models.TextField(blank=True, null=True)

    name_surname = models.CharField(max_length=100, null=True, blank=True)
    phone_number = models.ForeignKey(PhoneNumber, related_name="phone_number", on_delete=models.SET_NULL, null=True)

    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)

    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)
    deleted_at = models.DateField(blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    last_login_date = models.DateField(blank=True, null=True)

    is_corporate_user = models.BooleanField(blank=True, default=False)
    is_allow_email_notification = models.BooleanField(blank=True, default=False)
    is_allow_push_notification = models.BooleanField(blank=True, default=False)
    is_can_change_password = models.BooleanField(blank=True, default=False)
    is_get_mail = models.BooleanField(blank=True, default=False)

    delete_reason = models.TextField(null=True, blank=True)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)

    objects = CustomUserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'status', "created_at"]

    def __str__(self):
        return self.username + " / " + str(self.is_verified)

        # this methods are require to login super user from admin panel

    def has_perm(self, perm, obj=None):
        return self.is_staff

        # this methods are require to login super user from admin panel

    def has_module_perms(self, app_label):
        return self.is_staff

    class Meta:
        verbose_name_plural = "Users"
        db_table = "users"


class MyAuthBackend(object):

    def authenticate(self, email, password):
        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                return user
            else:
                return None
        except User.DoesNotExist:
            return None
        except Exception as e:
            return None

    def get_user(self, user_id):
        try:
            user = User.objects.get(sys_id=user_id)
            if user.is_active:
                return user
            return None
        except User.DoesNotExist:
            return None


class Company(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="company_user", on_delete=models.CASCADE)
    name = models.TextField()
    description = models.TextField(blank=True, null=True)
    logo = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    location = models.ForeignKey(Location, related_name="company_location", on_delete=models.SET_NULL, null=True)
    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Companies"
        db_table = "companies"


class CompanyImage(models.Model):
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    company = models.ForeignKey(Company, related_name="company_image", on_delete=models.CASCADE, blank=True,
                                null=True)
    order = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.company.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        image_list = CompanyImage.objects.filter(company_id=self.company_id)
        if self.order is None:
            if image_list is None or len(image_list) is 0:
                self.order = 1
            else:
                self.order = len(image_list) + 1

        super().save()

    class Meta:
        verbose_name_plural = "Company Images"
        db_table = "company_images"
        ordering = ['order']


class Branch(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="branch_user", on_delete=models.CASCADE)
    company = models.ForeignKey(Company, related_name="branch_company", on_delete=models.CASCADE)
    name = models.TextField()
    location = models.ForeignKey(Location, related_name="branch_location", on_delete=models.SET_NULL, null=True)
    working_time = models.TextField()
    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)
    status = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Branches"
        db_table = "branches"


class BranchComment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="branch_comment_user", on_delete=models.CASCADE)
    comment = models.TextField()
    branch = models.ForeignKey(Branch, related_name="branch_comment_branch", on_delete=models.CASCADE)
    comment_create_date = models.DateTimeField(auto_now_add=True, null=True)
    rate = models.FloatField(null=True)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Branch Comments"
        db_table = "branch_comment"
        ordering = ['-comment_create_date']

    def __unicode__(self):
        return str(self.comment)

    def __str__(self):
        return self.comment

    def children(self):
        return BranchComment.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True


class BranchPhoneNumber(models.Model):
    branch = models.ForeignKey(Branch, related_name="phone_number_branch", on_delete=models.CASCADE)
    country_code = models.CharField(max_length=5)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name_plural = "Branch Phone Numbers"
        db_table = "branch_phone_numbers"


class BranchProperty(models.Model):
    branch = models.ForeignKey(Branch, related_name="property_branch", on_delete=models.CASCADE)
    property = models.ForeignKey(Property, related_name="branch_property", on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} ".format(self.branch.name, self.property.name)

    class Meta:
        verbose_name_plural = "Branch Properties"
        db_table = "branch_properties"


class BranchImage(models.Model):
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    branch = models.ForeignKey(Branch, related_name="branch_image", on_delete=models.CASCADE)
    order = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.branch.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        image_list = BranchImage.objects.filter(branch_id=self.branch_id)

        if self.order is None:
            if image_list is None or len(image_list) is 0:
                self.order = 1
            else:
                self.order = len(image_list) + 1

        super().save()

    class Meta:
        verbose_name_plural = "Branch Images"
        db_table = "branch_images"
        ordering = ['order']


class BranchFollow(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="branch_follow_user", on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, related_name="branch_follow_branch", on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Branch Follow"
        db_table = "branch_follow"
        ordering = ['created_at']

    def __unicode__(self):
        return str(self.user.username)

    def __str__(self):
        return self.user.username


class BranchShare(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="branch_share_user", on_delete=models.SET_NULL,
                             null=True)
    branch = models.ForeignKey(Branch, related_name="branch_share_branch", on_delete=models.CASCADE)
    share_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Branch Share"
        db_table = "branch_share"
        ordering = ['share_date']

    def __unicode__(self):
        return str(self.user.username)

    def __str__(self):
        return self.user.username


class Menu(models.Model):
    branch = models.ForeignKey(Branch, related_name="menu_branch", on_delete=models.CASCADE)
    title = models.TextField()
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    status = models.SmallIntegerField(default=0)
    is_primary = models.BooleanField(default=True)
    category_count = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    product_count = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Menus"
        db_table = "menus"
        ordering = ["id"]


class QrCode(models.Model):
    menu = models.ForeignKey(Menu, related_name="code_menu", on_delete=models.CASCADE)
    content = models.TextField()
    image = models.ImageField(upload_to=get_file_path, null=True)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name_plural = "QrCodes"
        db_table = "qr_codes"


class QrCodeRead(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="code_read_user", on_delete=models.CASCADE,
                             null=True)
    menu = models.ForeignKey(Menu, related_name="code_read_menu", on_delete=models.CASCADE)
    qr_code = models.ForeignKey(QrCode, related_name="code_read_code", on_delete=models.CASCADE)
    latitude = models.CharField(max_length=100)
    longitude = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.qr_code.content

    class Meta:
        verbose_name_plural = "QrCodeReads"
        db_table = "qr_code_reads"
        ordering = ['created_at']


class Category(models.Model):
    menu = models.ForeignKey(Menu, related_name="category_menu", on_delete=models.CASCADE)
    title = models.TextField()
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    order = models.PositiveIntegerField(null=True, blank=True)
    product_count = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    status = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        category_list = Category.objects.filter(menu_id=self.menu.id)

        if self.order is None:
            if category_list is None or len(category_list) is 0:
                self.order = 1
            else:
                self.order = len(category_list) + 1

        super().save()

    class Meta:
        verbose_name_plural = "Categories"
        db_table = "categories"
        ordering = ['order']


class Product(models.Model):
    menu = models.ForeignKey(Menu, related_name="product_menu", on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name="product_category", on_delete=models.CASCADE)
    title = models.TextField()
    description = models.TextField()
    cover_image = models.ImageField(null=True, blank=True)
    calorie = models.PositiveIntegerField(default=1)
    order = models.PositiveIntegerField(null=True, blank=True)
    average_rate = models.FloatField(null=True)
    like_count = models.PositiveIntegerField(default=0)
    share_count = models.PositiveIntegerField(default=0)
    is_trend = models.BooleanField(default=False)
    is_should_taste = models.BooleanField(default=False)
    status = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        product_list = Product.objects.filter(category_id=self.category.id)

        if self.order is None:
            if product_list is None or len(product_list) is 0:
                self.order = 1
            else:
                self.order = len(product_list) + 1

        super().save()

    class Meta:
        verbose_name_plural = "Products"
        db_table = "products"
        ordering = ['order']


class ProductComment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="product_comment_user", on_delete=models.CASCADE,
                             null=True)
    comment = models.TextField()
    product = models.ForeignKey(Product, related_name="product_comment", on_delete=models.CASCADE)
    comment_create_date = models.DateTimeField(auto_now_add=True, null=True)
    rate = models.FloatField(null=True)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Product Comments"
        db_table = "product_comment"
        ordering = ['-comment_create_date']

    def __unicode__(self):
        return str(self.comment)

    def __str__(self):
        return self.comment

    def children(self):
        return ProductComment.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True


class ProductFav(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="product_fav_user", on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name="product_fav_product", on_delete=models.CASCADE)
    fav_date = models.DateField(auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Product Fav"
        db_table = "product_fav"
        ordering = ['fav_date']

    def __unicode__(self):
        return str(self.user.username)

    def __str__(self):
        return self.user.username


class ProductShare(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="product_share_user", on_delete=models.SET_NULL,
                             null=True)
    product = models.ForeignKey(Product, related_name="product_share_product", on_delete=models.CASCADE)
    share_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Product Share"
        db_table = "product_share"
        ordering = ['share_date']

    def __unicode__(self):
        return str(self.user.username)

    def __str__(self):
        return self.user.username


class ProductImage(models.Model):
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    product = models.ForeignKey(Product, related_name="product_image", on_delete=models.CASCADE)
    order = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.product.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        image_list = ProductImage.objects.filter(product_id=self.product_id)

        if self.order is None:
            if image_list is None or len(image_list) is 0:
                self.order = 1
            else:
                self.order = len(image_list) + 1

        super().save()

    class Meta:
        verbose_name_plural = "Product Images"
        db_table = "product_images"
        ordering = ['order']


class Ingredient(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "Ingredients"
        db_table = "ingredients"


class ProductIngredient(models.Model):
    product = models.ForeignKey(Product, related_name="ingredient_product_relation", on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, related_name="product_ingredient_relation", on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.ingredient.name)

    class Meta:
        verbose_name_plural = "Product Ingredients"
        db_table = "product_ingredients"


class Price(models.Model):
    product = models.ForeignKey(Product, related_name="price_product", on_delete=models.CASCADE)
    amount = models.FloatField()
    currency = models.CharField(max_length=5)

    def __str__(self):
        return "{} {}".format(self.amount, self.currency)

    class Meta:
        verbose_name_plural = "Prices"
        db_table = "prices"


class PreparingTime(models.Model):
    product = models.ForeignKey(Product, related_name="preparing_time_product", on_delete=models.CASCADE)
    min_value = models.PositiveSmallIntegerField()
    max_value = models.PositiveSmallIntegerField()

    def __str__(self):
        return "{} - {}".format(self.min_value, self.max_value)

    class Meta:
        verbose_name_plural = "Preparing Times"
        db_table = "preparing_times"


class Feedback(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="feedback_user", on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateField(auto_now_add=True, blank=True)

    def __str__(self):
        return "{}".format(self.message)

    class Meta:
        verbose_name_plural = "Feedbacks"
        db_table = "feedbacks"


class CacheTime(models.Model):
    name = models.TextField()
    type = models.PositiveSmallIntegerField()
    time = models.FloatField()
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name_plural = "CacheTimes"
        db_table = "cache_time"


class DiscountCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="discountcode_user", on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, related_name="discountcode_branch", on_delete=models.CASCADE)
    qr_code = models.ForeignKey(QrCode, related_name="discountcode_qrcode", on_delete=models.CASCADE)
    status = models.SmallIntegerField(default=1)
    discount_code = models.CharField(max_length=8)
    device_code = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "DiscountCodes"
        db_table = "discount_codes"
        ordering = ["-created_at"]


class Notification(models.Model):
    TYPE_DEFAULT = 0
    TYPE_CAMPAIGN = 1
    TYPE_BRANCH = 2
    TYPE_CHOICES = (
        (TYPE_CAMPAIGN, 'Campaign'),
        (TYPE_BRANCH, 'Branch')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="notification_user", on_delete=models.CASCADE)
    routing_id = models.IntegerField(null=True, blank=True)
    routing_url = models.TextField(null=True, blank=True)
    type = models.IntegerField(choices=TYPE_CHOICES, default=TYPE_DEFAULT, )
    type_id = models.IntegerField()
    text = models.TextField()
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    sending_at = models.DateField(auto_now_add=True, blank=True)
    is_read = models.BooleanField(default=False)
    is_send = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Notifications"
        db_table = "notifications"


class Campaign(models.Model):
    product = models.ForeignKey(Product, related_name="campaign_product", on_delete=models.CASCADE)
    discount_rate = models.IntegerField()
    start_date = models.DateField(auto_now_add=True, blank=True)
    end_date = models.DateField()
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Campaigns"
        db_table = "campaigns"


# --- MANUEL DELETE IMAGE AND FILE FIELD #
@receiver(models.signals.pre_delete, sender=Company)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    print("Company Delete", instance.logo)
    if instance.logo:
        print("Instance LOGO", instance.logo)
        if os.path.isfile(instance.logo.path):
            os.remove(instance.logo.path)

    for companyImage in instance.company_image.all():
        if os.path.isfile(companyImage.image.path):
            os.remove(companyImage.image.path)


@receiver(models.signals.pre_save, sender=Company)
def auto_delete_file_on_change(sender, instance, **kwargs):
    print("Before", instance)
    if not instance.pk:
        return False

    try:
        old_file = Company.objects.get(pk=instance.pk).logo
    except Company.DoesNotExist:
        return False

    if os.path.isfile(old_file.path):
        os.remove(old_file.path)


@receiver(models.signals.pre_save, sender=Menu)
def auto_delete_file_on_change(sender, instance, **kwargs):
    if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
        setattr(instance, _UNSAVED_FILEFIELD, instance.image)
        instance.image = None

    if not instance.pk:
        return False

    try:
        old_file = Menu.objects.get(pk=instance.pk).image
    except Menu.DoesNotExist:
        return False

    if old_file:
        new_file = instance.image
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)


_UNSAVED_FILEFIELD = 'unsaved_filefield'


@receiver(models.signals.post_save, sender=Menu)
def update_file_path(instance, created, **kwargs):
    if created and hasattr(instance, _UNSAVED_FILEFIELD):
        instance.image = getattr(instance, _UNSAVED_FILEFIELD)
        instance.save()
        # delete it if you feel uncomfortable...
        # instance.__dict__.pop(_UNSAVED_FILEFIELD)


@receiver(models.signals.pre_save, sender=Category)
def auto_delete_file_on_change(sender, instance, **kwargs):
    if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
        setattr(instance, _UNSAVED_FILEFIELD, instance.image)
        instance.image = None

    if not instance.pk:
        return False

    try:
        old_file = Category.objects.get(pk=instance.pk).image
    except Category.DoesNotExist:
        return False

    if old_file:
        new_file = instance.image
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)


@receiver(models.signals.post_save, sender=Category)
def update_file_path(instance, created, **kwargs):
    if created and hasattr(instance, _UNSAVED_FILEFIELD):
        instance.image = getattr(instance, _UNSAVED_FILEFIELD)
        instance.save()
        # delete it if you feel uncomfortable...
        # instance.__dict__.pop(_UNSAVED_FILEFIELD)


@receiver(models.signals.pre_delete, sender=CompanyImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    print("Company Image Pre Delete")
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_delete, sender=BranchImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_delete, sender=ProductImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)
